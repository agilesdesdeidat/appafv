package com.example.appafv.Model;

public class MotivoSin {

    private Integer motivosId;
    private String descripcion;

    public MotivoSin(Integer motivosId, String descripcion) {
        this.motivosId = motivosId;
        this.descripcion = descripcion;
    }

    public Integer getMotivosId() {
        return motivosId;
    }

    public void setMotivosId(Integer motivosId) {
        this.motivosId = motivosId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
