package com.example.appafv.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Vehiculo implements Parcelable {

    private Integer vehiculoId;
    private String placa;
    private String Proveedor_proveedorId;
    private Double precio;
    private String marca;
    private Integer anioFabri;
    private String imgv;
    private String observacion;
    private Integer Estado_Vehiculo_estadovId;

    public Vehiculo() {
    }

    public Vehiculo(Integer vehiculoId, String placa, String proveedor_proveedorId, Double precio, String marca, Integer anioFabri, String imgv, String observacion, Integer estado_Vehiculo_estadovId) {
        this.vehiculoId = vehiculoId;
        this.placa = placa;
        Proveedor_proveedorId = proveedor_proveedorId;
        this.precio = precio;
        this.marca = marca;
        this.anioFabri = anioFabri;
        this.imgv = imgv;
        this.observacion = observacion;
        Estado_Vehiculo_estadovId = estado_Vehiculo_estadovId;
    }

    protected Vehiculo(Parcel in) {
        if (in.readByte() == 0) {
            vehiculoId = null;
        } else {
            vehiculoId = in.readInt();
        }
        placa = in.readString();
        Proveedor_proveedorId = in.readString();
        if (in.readByte() == 0) {
            precio = null;
        } else {
            precio = in.readDouble();
        }
        marca = in.readString();
        if (in.readByte() == 0) {
            anioFabri = null;
        } else {
            anioFabri = in.readInt();
        }
        imgv = in.readString();
        observacion = in.readString();
        if (in.readByte() == 0) {
            Estado_Vehiculo_estadovId = null;
        } else {
            Estado_Vehiculo_estadovId = in.readInt();
        }
    }

    public static final Creator<Vehiculo> CREATOR = new Creator<Vehiculo>() {
        @Override
        public Vehiculo createFromParcel(Parcel in) {
            return new Vehiculo(in);
        }

        @Override
        public Vehiculo[] newArray(int size) {
            return new Vehiculo[size];
        }
    };

    public Integer getVehiculoId() {
        return vehiculoId;
    }

    public void setVehiculoId(Integer vehiculoId) {
        this.vehiculoId = vehiculoId;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getProveedor_proveedorId() {
        return Proveedor_proveedorId;
    }

    public void setProveedor_proveedorId(String proveedor_proveedorId) {
        Proveedor_proveedorId = proveedor_proveedorId;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getAnioFabri() {
        return anioFabri;
    }

    public void setAnioFabri(Integer anioFabri) {
        this.anioFabri = anioFabri;
    }

    public String getImgv() {
        return imgv;
    }

    public void setImgv(String imgv) {
        this.imgv = imgv;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getEstado_Vehiculo_estadovId() {
        return Estado_Vehiculo_estadovId;
    }

    public void setEstado_Vehiculo_estadovId(Integer estado_Vehiculo_estadovId) {
        Estado_Vehiculo_estadovId = estado_Vehiculo_estadovId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (vehiculoId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(vehiculoId);
        }
        dest.writeString(placa);
        dest.writeString(Proveedor_proveedorId);
        if (precio == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(precio);
        }
        dest.writeString(marca);
        if (anioFabri == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(anioFabri);
        }
        dest.writeString(imgv);
        dest.writeString(observacion);
        if (Estado_Vehiculo_estadovId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Estado_Vehiculo_estadovId);
        }
    }

}
