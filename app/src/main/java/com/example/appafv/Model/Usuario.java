package com.example.appafv.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Usuario implements Parcelable {

    private Integer usuarioId;
    private String nombrec;
    private String username;
    private String password;
    private String rol;
    private String correo;
    private String imgu;
    private String estadou;

    public Usuario() {
    }

    public Usuario(Integer usuarioId, String nombrec, String username, String password, String rol, String correo, String imgu, String estadou) {
        this.usuarioId = usuarioId;
        this.nombrec = nombrec;
        this.username = username;
        this.password = password;
        this.rol = rol;
        this.correo = correo;
        this.imgu = imgu;
        this.estadou = estadou;
    }

    protected Usuario(Parcel in) {
        if (in.readByte() == 0) {
            usuarioId = null;
        } else {
            usuarioId = in.readInt();
        }
        nombrec = in.readString();
        username = in.readString();
        password = in.readString();
        rol = in.readString();
        correo = in.readString();
        imgu = in.readString();
        estadou = in.readString();
    }

    public static final Creator<Usuario> CREATOR = new Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel in) {
            return new Usuario(in);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getNombrec() {
        return nombrec;
    }

    public void setNombrec(String nombrec) {
        this.nombrec = nombrec;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getImgu() {
        return imgu;
    }

    public void setImgu(String imgu) {
        this.imgu = imgu;
    }

    public String getEstadou() {
        return estadou;
    }

    public void setEstadou(String estadou) {
        this.estadou = estadou;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (usuarioId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(usuarioId);
        }
        dest.writeString(nombrec);
        dest.writeString(username);
        dest.writeString(password);
        dest.writeString(rol);
        dest.writeString(correo);
        dest.writeString(imgu);
        dest.writeString(estadou);
    }

}
