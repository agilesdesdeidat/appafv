package com.example.appafv.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Proveedor implements Parcelable {

    Integer proveedorId;
    String nombreEmp;
    String ruc;
    String telefono;
    String direccion;
    String imgp;

    public Proveedor() {
    }

    public Proveedor(Integer proveedorId, String nombreEmp, String ruc, String telefono, String direccion, String imgp) {
        this.proveedorId = proveedorId;
        this.nombreEmp = nombreEmp;
        this.ruc = ruc;
        this.telefono = telefono;
        this.direccion = direccion;
        this.imgp = imgp;
    }

    protected Proveedor(Parcel in) {
        if (in.readByte() == 0) {
            proveedorId = null;
        } else {
            proveedorId = in.readInt();
        }
        nombreEmp = in.readString();
        ruc = in.readString();
        telefono = in.readString();
        direccion = in.readString();
        imgp = in.readString();
    }

    public static final Creator<Proveedor> CREATOR = new Creator<Proveedor>() {
        @Override
        public Proveedor createFromParcel(Parcel in) {
            return new Proveedor(in);
        }

        @Override
        public Proveedor[] newArray(int size) {
            return new Proveedor[size];
        }
    };

    public Integer getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(Integer proveedorId) {
        this.proveedorId = proveedorId;
    }

    public String getNombreEmp() {
        return nombreEmp;
    }

    public void setNombreEmp(String nombreEmp) {
        this.nombreEmp = nombreEmp;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getImgp() {
        return imgp;
    }

    public void setImgp(String imgp) {
        this.imgp = imgp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (proveedorId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(proveedorId);
        }
        dest.writeString(nombreEmp);
        dest.writeString(ruc);
        dest.writeString(telefono);
        dest.writeString(direccion);
        dest.writeString(imgp);
    }

}
