package com.example.appafv.Model;

public class Empleado {

    private Integer empleadoId;
    private String nombrec;

    public Empleado(Integer empleadoId, String nombrec) {
        this.empleadoId = empleadoId;
        this.nombrec = nombrec;
    }

    public Integer getEmpleadoId() {
        return empleadoId;
    }

    public void setEmpleadoId(Integer empleadoId) {
        this.empleadoId = empleadoId;
    }

    public String getNombrec() {
        return nombrec;
    }

    public void setNombrec(String nombrec) {
        this.nombrec = nombrec;
    }

}
