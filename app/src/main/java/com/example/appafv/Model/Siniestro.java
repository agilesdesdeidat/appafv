package com.example.appafv.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Siniestro implements Parcelable {

    private Integer siniestroId;
    private String Vehiculo_vehiculoId;
    private String Empleado_empleadoId;
    private String fecha;
    private String tipo;
    private String Motivo_Siniestro_motivosId;
    private String imgs;
    private String detalle;

    public Siniestro() {
    }

    public Siniestro(Integer siniestroId, String vehiculo_vehiculoId, String empleado_empleadoId, String fecha, String tipo, String motivo_Siniestro_motivosId, String imgs, String detalle) {
        this.siniestroId = siniestroId;
        Vehiculo_vehiculoId = vehiculo_vehiculoId;
        Empleado_empleadoId = empleado_empleadoId;
        this.fecha = fecha;
        this.tipo = tipo;
        Motivo_Siniestro_motivosId = motivo_Siniestro_motivosId;
        this.imgs = imgs;
        this.detalle = detalle;
    }

    protected Siniestro(Parcel in) {
        siniestroId = in.readInt();
        Vehiculo_vehiculoId = in.readString();
        Empleado_empleadoId = in.readString();
        fecha = in.readString();
        tipo = in.readString();
        Motivo_Siniestro_motivosId = in.readString();
        imgs = in.readString();
        detalle = in.readString();
    }

    public static final Creator<Siniestro> CREATOR = new Creator<Siniestro>() {
        @Override
        public Siniestro createFromParcel(Parcel in) {
            return new Siniestro(in);
        }

        @Override
        public Siniestro[] newArray(int size) {
            return new Siniestro[size];
        }
    };

    public Integer getSiniestroId() {
        return siniestroId;
    }

    public void setSiniestroId(Integer siniestroId) {
        this.siniestroId = siniestroId;
    }

    public String getVehiculo_vehiculoId() {
        return Vehiculo_vehiculoId;
    }

    public void setVehiculo_vehiculoId(String vehiculo_vehiculoId) {
        Vehiculo_vehiculoId = vehiculo_vehiculoId;
    }

    public String getEmpleado_empleadoId() {
        return Empleado_empleadoId;
    }

    public void setEmpleado_empleadoId(String empleado_empleadoId) {
        Empleado_empleadoId = empleado_empleadoId;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMotivo_Siniestro_motivosId() {
        return Motivo_Siniestro_motivosId;
    }

    public void setMotivo_Siniestro_motivosId(String motivo_Siniestro_motivosId) {
        Motivo_Siniestro_motivosId = motivo_Siniestro_motivosId;
    }

    public String getImgs() {
        return imgs;
    }

    public void setImgs(String imgs) {
        this.imgs = imgs;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(siniestroId);
        dest.writeString(Vehiculo_vehiculoId);
        dest.writeString(Empleado_empleadoId);
        dest.writeString(fecha);
        dest.writeString(tipo);
        dest.writeString(Motivo_Siniestro_motivosId);
        dest.writeString(imgs);
        dest.writeString(detalle);
    }

}
