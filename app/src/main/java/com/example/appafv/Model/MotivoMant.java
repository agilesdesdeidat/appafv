package com.example.appafv.Model;

public class MotivoMant {

    private Integer motivomId;
    private String descripcion;

    public MotivoMant(Integer motivomId, String descripcion) {
        this.motivomId = motivomId;
        this.descripcion = descripcion;
    }

    public Integer getMotivomId() {
        return motivomId;
    }

    public void setMotivomId(Integer motivomId) {
        this.motivomId = motivomId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
