package com.example.appafv.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ControlVehiculo implements Parcelable {

    private Integer controlId;
    private String Vehiculo_vehiculoId;
    private String Empleado_empleadoId;
    private String fecha;
    private String horasalida;
    private String horaentrada;
    private String imgv;
    private Integer Estado_Control_estadocId;

    public ControlVehiculo() {

    }

    public ControlVehiculo(Integer controlId, String vehiculo_vehiculoId, String empleado_empleadoId, String fecha, String horasalida, String horaentrada, String imgv, Integer estado_Control_estadocId) {
        this.controlId = controlId;
        Vehiculo_vehiculoId = vehiculo_vehiculoId;
        Empleado_empleadoId = empleado_empleadoId;
        this.fecha = fecha;
        this.horasalida = horasalida;
        this.horaentrada = horaentrada;
        this.imgv = imgv;
        Estado_Control_estadocId = estado_Control_estadocId;
    }

    protected ControlVehiculo(Parcel in) {
        if (in.readByte() == 0) {
            controlId = null;
        } else {
            controlId = in.readInt();
        }
        Vehiculo_vehiculoId = in.readString();
        Empleado_empleadoId = in.readString();
        fecha = in.readString();
        horasalida = in.readString();
        horaentrada = in.readString();
        imgv = in.readString();
        if (in.readByte() == 0) {
            Estado_Control_estadocId = null;
        } else {
            Estado_Control_estadocId = in.readInt();
        }
    }

    public static final Creator<ControlVehiculo> CREATOR = new Creator<ControlVehiculo>() {
        @Override
        public ControlVehiculo createFromParcel(Parcel in) {
            return new ControlVehiculo(in);
        }

        @Override
        public ControlVehiculo[] newArray(int size) {
            return new ControlVehiculo[size];
        }
    };

    public Integer getControlId() {
        return controlId;
    }

    public void setControlId(Integer controlId) {
        this.controlId = controlId;
    }

    public String getVehiculo_vehiculoId() {
        return Vehiculo_vehiculoId;
    }

    public void setVehiculo_vehiculoId(String vehiculo_vehiculoId) {
        Vehiculo_vehiculoId = vehiculo_vehiculoId;
    }

    public String getEmpleado_empleadoId() {
        return Empleado_empleadoId;
    }

    public void setEmpleado_empleadoId(String empleado_empleadoId) {
        Empleado_empleadoId = empleado_empleadoId;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHorasalida() {
        return horasalida;
    }

    public void setHorasalida(String horasalida) {
        this.horasalida = horasalida;
    }

    public String getHoraentrada() {
        return horaentrada;
    }

    public void setHoraentrada(String horaentrada) {
        this.horaentrada = horaentrada;
    }

    public String getImgv() {
        return imgv;
    }

    public void setImgv(String imgv) {
        this.imgv = imgv;
    }

    public Integer getEstado_Control_estadocId() {
        return Estado_Control_estadocId;
    }

    public void setEstado_Control_estadocId(Integer estado_Control_estadocId) {
        Estado_Control_estadocId = estado_Control_estadocId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (controlId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(controlId);
        }
        dest.writeString(Vehiculo_vehiculoId);
        dest.writeString(Empleado_empleadoId);
        dest.writeString(fecha);
        dest.writeString(horasalida);
        dest.writeString(horaentrada);
        dest.writeString(imgv);
        if (Estado_Control_estadocId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Estado_Control_estadocId);
        }
    }

}
