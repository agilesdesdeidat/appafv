package com.example.appafv.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Mantenimiento implements Parcelable {

    private Integer mantenimientoId;
    private String Vehiculo_vehiculoId;
    private String Empleado_empleadoId;
    private String fecha;
    private String Tipo_Mant_tipomId;
    private String Motivo_Mant_motivomId;
    private Double precio;
    private String imgm;
    private String observacion;
    private Integer Estado_Mant_estadomId;

    public Mantenimiento() {
    }

    public Mantenimiento(Integer mantenimientoId, String vehiculo_vehiculoId, String empleado_empleadoId, String fecha, String tipo_Mant_tipomId, String motivo_Mant_motivomId, Double precio, String imgm, String observacion, Integer estado_Mant_estadomId) {
        this.mantenimientoId = mantenimientoId;
        Vehiculo_vehiculoId = vehiculo_vehiculoId;
        Empleado_empleadoId = empleado_empleadoId;
        this.fecha = fecha;
        Tipo_Mant_tipomId = tipo_Mant_tipomId;
        Motivo_Mant_motivomId = motivo_Mant_motivomId;
        this.precio = precio;
        this.imgm = imgm;
        this.observacion = observacion;
        Estado_Mant_estadomId = estado_Mant_estadomId;
    }

    protected Mantenimiento(Parcel in) {
        if (in.readByte() == 0) {
            mantenimientoId = null;
        } else {
            mantenimientoId = in.readInt();
        }
        Vehiculo_vehiculoId = in.readString();
        Empleado_empleadoId = in.readString();
        fecha = in.readString();
        Tipo_Mant_tipomId = in.readString();
        Motivo_Mant_motivomId = in.readString();
        if (in.readByte() == 0) {
            precio = null;
        } else {
            precio = in.readDouble();
        }
        imgm = in.readString();
        observacion = in.readString();
        if (in.readByte() == 0) {
            Estado_Mant_estadomId = null;
        } else {
            Estado_Mant_estadomId = in.readInt();
        }
    }

    public static final Creator<Mantenimiento> CREATOR = new Creator<Mantenimiento>() {
        @Override
        public Mantenimiento createFromParcel(Parcel in) {
            return new Mantenimiento(in);
        }

        @Override
        public Mantenimiento[] newArray(int size) {
            return new Mantenimiento[size];
        }
    };

    public Integer getMantenimientoId() {
        return mantenimientoId;
    }

    public void setMantenimientoId(Integer mantenimientoId) {
        this.mantenimientoId = mantenimientoId;
    }

    public String getVehiculo_vehiculoId() {
        return Vehiculo_vehiculoId;
    }

    public void setVehiculo_vehiculoId(String vehiculo_vehiculoId) {
        Vehiculo_vehiculoId = vehiculo_vehiculoId;
    }

    public String getEmpleado_empleadoId() {
        return Empleado_empleadoId;
    }

    public void setEmpleado_empleadoId(String empleado_empleadoId) {
        Empleado_empleadoId = empleado_empleadoId;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipo_Mant_tipomId() {
        return Tipo_Mant_tipomId;
    }

    public void setTipo_Mant_tipomId(String tipo_Mant_tipomId) {
        Tipo_Mant_tipomId = tipo_Mant_tipomId;
    }

    public String getMotivo_Mant_motivomId() {
        return Motivo_Mant_motivomId;
    }

    public void setMotivo_Mant_motivomId(String motivo_Mant_motivomId) {
        Motivo_Mant_motivomId = motivo_Mant_motivomId;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getImgm() {
        return imgm;
    }

    public void setImgm(String imgm) {
        this.imgm = imgm;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getEstado_Mant_estadomId() {
        return Estado_Mant_estadomId;
    }

    public void setEstado_Mant_estadomId(Integer estado_Mant_estadomId) {
        Estado_Mant_estadomId = estado_Mant_estadomId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (mantenimientoId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(mantenimientoId);
        }
        dest.writeString(Vehiculo_vehiculoId);
        dest.writeString(Empleado_empleadoId);
        dest.writeString(fecha);
        dest.writeString(Tipo_Mant_tipomId);
        dest.writeString(Motivo_Mant_motivomId);
        if (precio == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(precio);
        }
        dest.writeString(imgm);
        dest.writeString(observacion);
        if (Estado_Mant_estadomId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Estado_Mant_estadomId);
        }
    }

}
