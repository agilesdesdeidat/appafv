package com.example.appafv.Model;

public class TipoMant {

    private Integer tipomId;
    private String descripcion;

    public TipoMant(Integer tipomId, String descripcion) {
        this.tipomId = tipomId;
        this.descripcion = descripcion;
    }

    public Integer getTipomId() {
        return tipomId;
    }

    public void setTipomId(Integer tipomId) {
        this.tipomId = tipomId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
