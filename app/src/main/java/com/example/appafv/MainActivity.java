package com.example.appafv;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.view.View;
import android.widget.Toast;
import com.example.appafv.ROOM.AppDatabase;
import com.example.appafv.ROOM.Entidad_User;
import com.example.appafv.Utils.LoginApiAdapter;
import com.example.appafv.Utils.LoginApiService;
import com.example.appafv.databinding.ActivityMainBinding;
import java.io.UnsupportedEncodingException;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private String user;
    private String pass;
    private String authToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppDatabase db = Room.databaseBuilder(this,
                AppDatabase.class, "AppAFV").
                allowMainThreadQueries().enableMultiInstanceInvalidation().
                fallbackToDestructiveMigration().build();

        List<Entidad_User> listaROOM = db.daoUser().registro();

        if (listaROOM.isEmpty()){
            Entidad_User lista = new Entidad_User();
            lista.setUsuario("local");
            db.daoUser().insert(lista);
        }

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user = binding.etUserLogin.getText().toString();
                pass = binding.etPassLogin.getText().toString();
                authToken = createAuthToken(user, pass);
                checkLoginDetail(authToken);
            }
        });
    }

    private String createAuthToken(String user, String pass) {
        byte []data = new byte[0];
        try {
            data = (user + ":" + pass).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    private void checkLoginDetail(String authToken) {
        Retrofit retrofit = LoginApiAdapter.getRetrofitInstance();
        final LoginApiService api = retrofit.create(LoginApiService.class);
        Call<String> call = api.valLogin(authToken);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body().matches("success")) {
                    binding.btnIngresar.setText("ESPERE POR FAVOR...");
                    binding.pbLogin.setVisibility(View.VISIBLE);

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            binding.btnIngresar.setText("INGRESAR");
                            binding.pbLogin.setVisibility(View.INVISIBLE);

                            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                                    AppDatabase.class, "AppAFV").
                                    allowMainThreadQueries().enableMultiInstanceInvalidation().
                                    fallbackToDestructiveMigration().build();

                            db.daoUser().updateUsuario(binding.etUserLogin.getText().toString());

                            Intent userData = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                            startActivity(userData);
                        }
                    }, 3000);
                } else {
                    Toast.makeText(getApplication(), "Username y/o password incorrectos!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

}