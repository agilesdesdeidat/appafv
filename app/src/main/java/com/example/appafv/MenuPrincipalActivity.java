package com.example.appafv;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.Model.Usuario;
import com.example.appafv.ROOM.AppDatabase;
import com.example.appafv.Utils.LoginApiService;
import com.google.android.material.navigation.NavigationView;
import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;
import java.util.List;
import java.util.UUID;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MenuPrincipalActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    private TextView tvMenuUser, tvMenuEmail, tvMenuNom, tvMenuRol;
    private ImageView ivMenuImg;

    String rolDefinitivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        View hview = navigationView.getHeaderView(0);
        tvMenuUser = hview.findViewById(R.id.tvMenuUser);
        tvMenuNom = hview.findViewById(R.id.tvMenuNom);
        tvMenuEmail = hview.findViewById(R.id.tvMenuEmail);
        tvMenuRol = hview.findViewById(R.id.tvMenuRol);
        ivMenuImg = hview.findViewById(R.id.ivMenuImg);

        AppDatabase db = Room.databaseBuilder(this,
                AppDatabase.class, "AppAFV").
                allowMainThreadQueries().enableMultiInstanceInvalidation().
                fallbackToDestructiveMigration().build();

        String text = db.daoUser().usuario();

        tvMenuUser.setText(text);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://taitacompany.000webhostapp.com/api_final/usuario/getuser/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        LoginApiService loginApiService = retrofit.create(LoginApiService.class);
        Call<List<Usuario>> call = loginApiService.getUserLogin(tvMenuUser.getText().toString());
        call.enqueue(new Callback<List<Usuario>>() {
            @Override
            public void onResponse(Call<List<Usuario>> call, Response<List<Usuario>> response) {
                if (!response.isSuccessful()) {
                    tvMenuNom.setText("code: " + response.code());
                    return;
                }

                List<Usuario> user = response.body();

                for (Usuario u : user) {
                    tvMenuNom.setText(u.getNombrec());
                    tvMenuEmail.setText(u.getCorreo());
                    tvMenuRol.setText(u.getRol());
                    rolDefinitivo = u.getRol();
                    Glide.with(getApplication().getApplicationContext())
                            .load(u.getImgu()).apply(RequestOptions.circleCropTransform())
                            .signature(new ObjectKey(UUID.randomUUID().toString()))
                            .into(ivMenuImg);
                }

                if (rolDefinitivo.equals("Almacenero")) {
                    Menu nav_Menu = navigationView.getMenu();
                    nav_Menu.removeGroup(R.id.menu_proveedor);
                    nav_Menu.removeGroup(R.id.menu_mant);
                    nav_Menu.removeGroup(R.id.menu_control);
                    nav_Menu.removeGroup(R.id.menu_user);
                    nav_Menu.removeGroup(R.id.menu_system);
                } else if (rolDefinitivo.equals("Controlador")) {
                    Menu nav_Menu = navigationView.getMenu();
                    nav_Menu.removeGroup(R.id.menu_mant);
                    nav_Menu.removeGroup(R.id.menu_proveedor);
                    nav_Menu.removeGroup(R.id.menu_sin);
                    nav_Menu.removeGroup(R.id.menu_user);
                    nav_Menu.removeGroup(R.id.menu_system);
                    nav_Menu.removeItem(R.id.registrarVehiculoFragment);
                } else if (rolDefinitivo.equals("Mecanico")) {
                    Menu nav_Menu = navigationView.getMenu();
                    nav_Menu.removeGroup(R.id.menu_proveedor);
                    nav_Menu.removeGroup(R.id.menu_control);
                    nav_Menu.removeGroup(R.id.menu_sin);
                    nav_Menu.removeGroup(R.id.menu_user);
                    nav_Menu.removeGroup(R.id.menu_system);
                    nav_Menu.removeItem(R.id.registrarVehiculoFragment);
                }
            }

            @Override
            public void onFailure(Call<List<Usuario>> call, Throwable t) {
                tvMenuNom.setText(t.getMessage());
            }
        });

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.listarProveedorFragment, R.id.registrarProveedorFragment, R.id.listarVehiculoActivoFragment,
                R.id.listarVehiculoInactivoFragment, R.id.registrarVehiculoFragment, R.id.listarMantPendienteFragment,
                R.id.listarMantRealizadoFragment, R.id.registrarMantFragment, R.id.listarControlEnCursoFragment,
                R.id.listarControlTerminadoFragment, R.id.registrarControlFragment, R.id.listarSiniestroFragment,
                R.id.registrarSiniestroFragment, R.id.listarUsuarioFragment, R.id.registrarUsuarioFragment,
                R.id.generarBackupDatos)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnCerrarSesion:
                startActivity(new Intent(getApplication().getApplicationContext(), MainActivity.class));
                finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

}