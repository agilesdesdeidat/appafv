package com.example.appafv.Utils;

import com.example.appafv.Model.MotivoMant;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface MotivoMantApiService {

    @GET("get")
    Call<ArrayList<MotivoMant>> getMotivoMant();

}
