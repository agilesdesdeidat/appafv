package com.example.appafv.Utils;

import com.example.appafv.Model.TipoMant;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface TipoMantApiService {

    @GET("get")
    Call<ArrayList<TipoMant>> getTipoMant();

}
