package com.example.appafv.Utils;

import com.example.appafv.Model.Siniestro;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SiniestroApiService {

    @GET("get")
    Call<ArrayList<Siniestro>> getSiniestros();

    @POST("post")
    Call<Siniestro> addSiniestro (@Body Siniestro siniestro);

    @POST("put/{id}")
    Call<Siniestro> updSiniestro (@Path("id") Integer id, @Body Siniestro siniestro);

}
