package com.example.appafv.Utils;

import com.example.appafv.Model.Empleado;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface EmpleadoApiService {

    @GET("get/mecanico")
    Call<ArrayList<Empleado>> getEmpleadoMecanico();

    @GET("get/conductor")
    Call<ArrayList<Empleado>> getEmpleadoConductor();

}
