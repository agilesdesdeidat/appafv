package com.example.appafv.Utils;

import retrofit2.Call;
import retrofit2.http.POST;

public interface SistemaApiService {

    @POST("backup")
    Call<String> createBackup();

}
