package com.example.appafv.Utils;

import com.example.appafv.Model.Proveedor;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ProveedorApiService {

    @GET("get")
    Call<ArrayList<Proveedor>> getProveedores();

    @POST("post")
    Call<Proveedor> addProveedor (@Body Proveedor proveedor);

    @POST("put/{id}")
    Call<Proveedor> updProveedor (@Path("id") Integer id, @Body Proveedor proveedor);

}
