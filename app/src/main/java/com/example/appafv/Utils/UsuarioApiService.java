package com.example.appafv.Utils;

import com.example.appafv.Model.Usuario;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UsuarioApiService {

    @GET("get")
    Call<ArrayList<Usuario>> getUsuarios();

    @POST("post")
    Call<Usuario> addUsuario (@Body Usuario usuario);

    @POST("put/{id}")
    Call<Usuario> updUsuario (@Path("id") Integer id, @Body Usuario usuario);

}
