package com.example.appafv.Utils;

import com.example.appafv.Model.MotivoSin;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface MotivoSinApiService {

    @GET("get")
    Call<ArrayList<MotivoSin>> getMotivoSin();

}
