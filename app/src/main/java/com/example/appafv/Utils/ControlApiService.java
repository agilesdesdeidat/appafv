package com.example.appafv.Utils;

import com.example.appafv.Model.Control;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ControlApiService {

    @GET("get/encurso")
    Call<ArrayList<Control>> getControlEnCurso();

    @GET("get/terminado")
    Call<ArrayList<Control>> getControlTerminado();

    @POST("post")
    Call<Control> addControl (@Body Control control);

    @POST("put/{id}")
    Call<Control> updControl (@Path("id") Integer id, @Body Control control);

    @POST("delete/{id}")
    Call<Control> delControl (@Path("id") Integer id);

}
