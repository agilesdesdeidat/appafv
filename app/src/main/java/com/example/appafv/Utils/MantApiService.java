package com.example.appafv.Utils;

import com.example.appafv.Model.Mantenimiento;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MantApiService {

    @GET("get/pendiente")
    Call<ArrayList<Mantenimiento>> getMantenimientoPendiente();

    @GET("get/realizado")
    Call<ArrayList<Mantenimiento>> getMantenimientoRealizado();

    @POST("post")
    Call<Mantenimiento> addMant (@Body Mantenimiento mantenimiento);

    @POST("delete/{id}")
    Call<Mantenimiento> delMant (@Path("id") Integer id);

    @POST("put/{id}")
    Call<Mantenimiento> updMant (@Path("id") Integer id, @Body Mantenimiento mantenimiento);

}
