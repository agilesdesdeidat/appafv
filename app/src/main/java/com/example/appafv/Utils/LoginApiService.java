package com.example.appafv.Utils;

import com.example.appafv.Model.Usuario;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface LoginApiService {

    @POST("login")
    Call<String> valLogin (@Header("Authorization") String authtoken);

    @GET("{usuario}")
    Call<List<Usuario>> getUserLogin (@Path("usuario") String usuario);

}
