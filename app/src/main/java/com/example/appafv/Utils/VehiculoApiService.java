package com.example.appafv.Utils;

import com.example.appafv.Model.Vehiculo;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface VehiculoApiService {

    @GET("get")
    Call<ArrayList<Vehiculo>> getVehiculos();

    @GET("get/disponible")
    Call<ArrayList<Vehiculo>> getVehiculoDisponible();

    @GET("get/nodisponible")
    Call<ArrayList<Vehiculo>> getVehiculoNoDisponible();

    @POST("post")
    Call<Vehiculo> addVehiculo (@Body Vehiculo vehiculo);

    @POST("put/{id}")
    Call<Vehiculo> updVehiculo (@Path("id") Integer id, @Body Vehiculo vehiculo);

}
