package com.example.appafv.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.Fragments.Vehiculo.DetalleVehiculo;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.R;
import java.util.ArrayList;
import java.util.UUID;

public class VehiculoAdapter extends RecyclerView.Adapter<VehiculoAdapter.ViewHolder> {

    Context context;
    ArrayList<Vehiculo> listvehiculo;

    public VehiculoAdapter(Context context, ArrayList<Vehiculo> listvehiculo) {
        this.context = context;
        this.listvehiculo = listvehiculo;
    }

    @NonNull
    @Override
    public VehiculoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VehiculoAdapter.ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_vehiculo, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VehiculoAdapter.ViewHolder holder, int position) {
        final Vehiculo vehiculo = listvehiculo.get(position);
        holder.tvVehiculoId.setText(vehiculo.getVehiculoId().toString());
        holder.tvVehiculoPla.setText(vehiculo.getPlaca());
        holder.tvVehiculoProv.setText(vehiculo.getProveedor_proveedorId());
        Glide.with(context)
                .load(vehiculo.getImgv())
                .signature(new ObjectKey(UUID.randomUUID().toString()))
                .into(holder.ivVehiculoImg);
        holder.contenedorVeh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detallev = new Intent(context, DetalleVehiculo.class);
                detallev.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                detallev.putExtra("detalleVehiculo", vehiculo);
                context.startActivity(detallev);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listvehiculo.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvVehiculoId, tvVehiculoProv, tvVehiculoPla;
        private ImageView ivVehiculoImg;
        private CardView contenedorVeh;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvVehiculoId = itemView.findViewById(R.id.tvVehiculoId);
            tvVehiculoProv = itemView.findViewById(R.id.tvVehiculoProv);
            tvVehiculoPla = itemView.findViewById(R.id.tvVehiculoPlaca);
            ivVehiculoImg = itemView.findViewById(R.id.ivVehiculoImg);
            contenedorVeh = itemView.findViewById(R.id.contenedorVeh);
        }
    }

}
