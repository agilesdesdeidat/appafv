package com.example.appafv.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.Fragments.Siniestro.DetalleSiniestro;
import com.example.appafv.Model.Siniestro;
import com.example.appafv.R;
import java.util.ArrayList;
import java.util.UUID;

public class SiniestroAdapter extends RecyclerView.Adapter<SiniestroAdapter.ViewHolder> {

    Context context;
    ArrayList<Siniestro> listsin;

    public SiniestroAdapter(Context context, ArrayList<Siniestro> listsin) {
        this.context = context;
        this.listsin = listsin;
    }

    @NonNull
    @Override
    public SiniestroAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SiniestroAdapter.ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_siniestro, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SiniestroAdapter.ViewHolder holder, int position) {
        final Siniestro objSiniestro = listsin.get(position);
        holder.tvSiniestroVeh.setText(objSiniestro.getVehiculo_vehiculoId());
        holder.tvSiniestroEmp.setText(objSiniestro.getEmpleado_empleadoId());
        holder.tvSiniestroTipo.setText(objSiniestro.getTipo());
        holder.tvSiniestroFecha.setText(objSiniestro.getFecha());
        Glide.with(context)
                .load(objSiniestro.getImgs())
                .signature(new ObjectKey(UUID.randomUUID().toString()))
                .into(holder.ivSiniestro);
        holder.contenedorSiniestro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detalleSin = new Intent(context, DetalleSiniestro.class);
                detalleSin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                detalleSin.putExtra("detalleSin", objSiniestro);
                context.startActivity(detalleSin);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listsin.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvSiniestroVeh, tvSiniestroEmp, tvSiniestroFecha, tvSiniestroTipo;
        private ImageView ivSiniestro;
        private CardView contenedorSiniestro;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvSiniestroVeh = itemView.findViewById(R.id.tvSiniestroVeh);
            tvSiniestroEmp = itemView.findViewById(R.id.tvSiniestroEmp);
            tvSiniestroFecha = itemView.findViewById(R.id.tvSiniestroFecha);
            tvSiniestroTipo = itemView.findViewById(R.id.tvSiniestroTipo);
            ivSiniestro = itemView.findViewById(R.id.ivSiniestro);
            contenedorSiniestro = itemView.findViewById(R.id.contenedorSiniestro);
        }
    }

}
