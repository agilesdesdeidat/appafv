package com.example.appafv.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.appafv.Fragments.Mantenimiento.DetalleMant;
import com.example.appafv.Model.MantVehiculo;
import com.example.appafv.R;
import java.util.ArrayList;

public class MantAdapter extends RecyclerView.Adapter<MantAdapter.ViewHolder> {

    Context context;
    ArrayList<MantVehiculo> listmants;

    public MantAdapter(Context context, ArrayList<MantVehiculo> listmants) {
        this.context = context;
        this.listmants = listmants;
    }

    @NonNull
    @Override
    public MantAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MantAdapter.ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_mant, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MantAdapter.ViewHolder holder, int position) {
        final MantVehiculo objMantV = listmants.get(position);
        holder.tvMantVeh.setText(objMantV.getVehiculo_vehiculoId());
        holder.tvMantTipo.setText(objMantV.getTipo_Mant_tipomId());
        holder.tvMantMotivo.setText(objMantV.getMotivo_Mant_motivomId());
        holder.tvMantFecha.setText(objMantV.getFecha());
        Glide.with(context)
                .load(objMantV.getImgm())
                .into(holder.ivMantImg);
        holder.contenedorMant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detalleMant = new Intent(context, DetalleMant.class);
                detalleMant.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                detalleMant.putExtra("detalleMant", objMantV);
                context.startActivity(detalleMant);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listmants.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMantVeh, tvMantTipo, tvMantMotivo, tvMantFecha;
        private ImageView ivMantImg;
        private CardView contenedorMant;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvMantVeh = itemView.findViewById(R.id.tvMantVeh);
            tvMantTipo = itemView.findViewById(R.id.tvMantTipo);
            tvMantMotivo = itemView.findViewById(R.id.tvMantMotivo);
            tvMantFecha = itemView.findViewById(R.id.tvMantFecha);
            ivMantImg = itemView.findViewById(R.id.ivMantImg);
            contenedorMant = itemView.findViewById(R.id.contenedorMant);
        }
    }

}
