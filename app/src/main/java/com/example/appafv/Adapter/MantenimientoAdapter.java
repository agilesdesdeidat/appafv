package com.example.appafv.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.Fragments.Mantenimiento.DetalleMant;
import com.example.appafv.Model.Mantenimiento;
import com.example.appafv.R;
import java.util.ArrayList;
import java.util.UUID;

public class MantenimientoAdapter extends RecyclerView.Adapter<MantenimientoAdapter.ViewHolder> {

    Context context;
    ArrayList<Mantenimiento> listmants;

    public MantenimientoAdapter(Context context, ArrayList<Mantenimiento> listmants) {
        this.context = context;
        this.listmants = listmants;
    }

    @NonNull
    @Override
    public MantenimientoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MantenimientoAdapter.ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_mant, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MantenimientoAdapter.ViewHolder holder, int position) {
        final Mantenimiento objMant = listmants.get(position);
        holder.tvMantVeh.setText(objMant.getVehiculo_vehiculoId());
        holder.tvMantTipo.setText(objMant.getTipo_Mant_tipomId());
        holder.tvMantMotivo.setText(objMant.getMotivo_Mant_motivomId());
        holder.tvMantFecha.setText(objMant.getFecha());
        Glide.with(context)
                .load(objMant.getImgm())
                .signature(new ObjectKey(UUID.randomUUID().toString()))
                .into(holder.ivMantImg);
        holder.contenedorMant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detalleMant = new Intent(context, DetalleMant.class);
                detalleMant.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                detalleMant.putExtra("detalleMant", objMant);
                context.startActivity(detalleMant);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listmants.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMantVeh, tvMantTipo, tvMantMotivo, tvMantFecha;
        private ImageView ivMantImg;
        private CardView contenedorMant;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvMantVeh = itemView.findViewById(R.id.tvMantVeh);
            tvMantTipo = itemView.findViewById(R.id.tvMantTipo);
            tvMantMotivo = itemView.findViewById(R.id.tvMantMotivo);
            tvMantFecha = itemView.findViewById(R.id.tvMantFecha);
            ivMantImg = itemView.findViewById(R.id.ivMantImg);
            contenedorMant = itemView.findViewById(R.id.contenedorMant);
        }
    }

}
