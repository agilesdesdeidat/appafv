package com.example.appafv.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.Fragments.Usuario.DetalleUsuario;
import com.example.appafv.Model.Usuario;
import com.example.appafv.R;
import java.util.ArrayList;
import java.util.UUID;

public class UsuarioAdapter extends RecyclerView.Adapter<UsuarioAdapter.ViewHolder> {

    Context context;
    ArrayList<Usuario> listuser;

    public UsuarioAdapter(Context context, ArrayList<Usuario> listuser) {
        this.context = context;
        this.listuser = listuser;
    }

    @NonNull
    @Override
    public UsuarioAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UsuarioAdapter.ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_usuario, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UsuarioAdapter.ViewHolder holder, int position) {
        final Usuario objUser = listuser.get(position);
        holder.tvUserId.setText(objUser.getUsuarioId().toString());
        holder.tvUserNom.setText(objUser.getNombrec());
        holder.tvUserRol.setText(objUser.getRol());
        Glide.with(context)
                .load(objUser.getImgu()).apply(RequestOptions.circleCropTransform())
                .signature(new ObjectKey(UUID.randomUUID().toString()))
                .into(holder.ivUserImg);
        holder.contenedorUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detalleU = new Intent(context, DetalleUsuario.class);
                detalleU.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                detalleU.putExtra("detalleUsuario", objUser);
                context.startActivity(detalleU);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listuser.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvUserId, tvUserNom, tvUserRol;
        private ImageView ivUserImg;
        private CardView contenedorUser;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvUserId = itemView.findViewById(R.id.tvUserId);
            tvUserNom = itemView.findViewById(R.id.tvUserNom);
            tvUserRol = itemView.findViewById(R.id.tvUserRol);
            ivUserImg = itemView.findViewById(R.id.ivUserImg);
            contenedorUser = itemView.findViewById(R.id.contenedorUser);
        }
    }

}
