package com.example.appafv.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.Fragments.Proveedor.DetalleProveedor;
import com.example.appafv.Model.Proveedor;
import com.example.appafv.R;
import java.util.ArrayList;
import java.util.UUID;

public class ProveedorAdapter extends RecyclerView.Adapter<ProveedorAdapter.ViewHolder> {

    Context context;
    ArrayList<Proveedor> listproveedor;

    public ProveedorAdapter(Context context, ArrayList<Proveedor> listproveedor) {
        this.context = context;
        this.listproveedor = listproveedor;
    }

    @NonNull
    @Override
    public ProveedorAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProveedorAdapter.ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_proveedor, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProveedorAdapter.ViewHolder holder, int position) {
        final Proveedor proveedor = listproveedor.get(position);
        holder.tvProvedorId.setText(proveedor.getProveedorId().toString());
        holder.tvProveedorEmp.setText(proveedor.getNombreEmp());
        Glide.with(context)
                .load(proveedor.getImgp())
                .signature(new ObjectKey(UUID.randomUUID().toString()))
                .into(holder.ivProveedor);
        holder.contenedorProv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detallep = new Intent(context, DetalleProveedor.class);
                detallep.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                detallep.putExtra("detalleProveedor", proveedor);
                context.startActivity(detallep);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listproveedor.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvProvedorId, tvProveedorEmp;
        private ImageView ivProveedor;
        private CardView contenedorProv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvProvedorId = itemView.findViewById(R.id.tvProveedorId);
            tvProveedorEmp = itemView.findViewById(R.id.tvProveedorEmp);
            ivProveedor = itemView.findViewById(R.id.ivProveedor);
            contenedorProv = itemView.findViewById(R.id.contenedorProv);
        }
    }

}
