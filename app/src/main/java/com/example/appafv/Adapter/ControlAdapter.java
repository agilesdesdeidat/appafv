package com.example.appafv.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.Fragments.Control.DetalleControl;
import com.example.appafv.Model.Control;
import com.example.appafv.R;
import java.util.ArrayList;
import java.util.UUID;

public class ControlAdapter extends RecyclerView.Adapter<ControlAdapter.ViewHolder> {

    Context context;
    ArrayList<Control> listcont;

    public ControlAdapter(Context context, ArrayList<Control> listcont) {
        this.context = context;
        this.listcont = listcont;
    }

    @NonNull
    @Override
    public ControlAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ControlAdapter.ViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_control, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ControlAdapter.ViewHolder holder, int position) {
        final Control objControl = listcont.get(position);
        holder.tvControlPlaca.setText(objControl.getVehiculo_vehiculoId());
        holder.tvControlEmp.setText(objControl.getEmpleado_empleadoId());
        holder.tvControlFecha.setText(objControl.getFecha());
        Glide.with(context)
                .load(objControl.getImgv())
                .signature(new ObjectKey(UUID.randomUUID().toString()))
                .into(holder.ivControlImg);
        holder.contenedorControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detalleControl = new Intent(context, DetalleControl.class);
                detalleControl.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                detalleControl.putExtra("detalleControl", objControl);
                context.startActivity(detalleControl);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listcont.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvControlPlaca, tvControlEmp, tvControlFecha;
        private ImageView ivControlImg;
        private CardView contenedorControl;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvControlPlaca = itemView.findViewById(R.id.tvControlPlaca);
            tvControlEmp = itemView.findViewById(R.id.tvControlEmp);
            tvControlFecha = itemView.findViewById(R.id.tvControlFecha);
            ivControlImg = itemView.findViewById(R.id.ivControlImg);
            contenedorControl = itemView.findViewById(R.id.contenedorControl);
        }
    }

}
