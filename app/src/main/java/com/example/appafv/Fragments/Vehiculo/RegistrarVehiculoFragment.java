package com.example.appafv.Fragments.Vehiculo;

import androidx.lifecycle.ViewModelProvider;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Proveedor;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.Utils.ProveedorApiAdapter;
import com.example.appafv.Utils.VehiculoApiAdapter;
import com.example.appafv.databinding.RegistrarVehiculoFragmentBinding;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrarVehiculoFragment extends Fragment {

    private RegistrarVehiculoViewModel mViewModel;
    private RegistrarVehiculoFragmentBinding binding;
    private int IMG_REQUEST = 21;
    private Bitmap bitmap;

    public static RegistrarVehiculoFragment newInstance() {
        return new RegistrarVehiculoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = RegistrarVehiculoFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(RegistrarVehiculoViewModel.class);
        // TODO: Use the ViewModel

        obtenerNombreProveedor();
        obtenerMarcas();

        binding.ibCargaUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMG_REQUEST);
            }
        });

        binding.btnAddVehiculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etAddVehPla.getText().toString().isEmpty() || binding.etAddVehPla.getText().toString().length() != 7 ||
                        binding.etAddVehPre.getText().toString().isEmpty() || binding.etAddVehAnio.getText().toString().isEmpty() ||
                        binding.ivAddVehImg.getDrawable() == null) {
                    validarDatos();
                } else {
                    Vehiculo ve = new Vehiculo();
                    ve.setPlaca(binding.etAddVehPla.getText().toString());
                    ve.setProveedor_proveedorId(binding.spAddVehProv.getSelectedItem().toString());
                    ve.setPrecio(Double.parseDouble(binding.etAddVehPre.getText().toString()));
                    ve.setMarca(binding.spAddVehMarca.getSelectedItem().toString());
                    ve.setAnioFabri(Integer.parseInt(binding.etAddVehAnio.getText().toString()));
                    ve.setImgv(uploadImage());
                    ve.setObservacion(binding.etAddVehObs.getText().toString());
                    ve.setEstado_Vehiculo_estadovId(1);
                    addVehiculo(ve);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);
                binding.ivAddVehImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addVehiculo(Vehiculo ve) {
        Call<Vehiculo> call = VehiculoApiAdapter.getApiService().addVehiculo(ve);
        call.enqueue(new Callback<Vehiculo>() {
            @Override
            public void onResponse(Call<Vehiculo> call, Response<Vehiculo> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getContext().getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getContext().getApplicationContext(), "Vehiculo registrado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Vehiculo> call, Throwable t) {
            }
        });
    }

    private void obtenerMarcas() {
        List<String> listm = new ArrayList<>();
        listm.add("Chevrolet");
        listm.add("Hyundai");
        listm.add("Mercedes Benz");
        listm.add("Audi");
        listm.add("Nissan");
        listm.add("Kia");
        ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, listm);
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spAddVehMarca.setAdapter(spArrayAdapter);
    }

    private void obtenerNombreProveedor() {
        Call<ArrayList<Proveedor>> call = ProveedorApiAdapter.getApiService().getProveedores();
        call.enqueue(new Callback<ArrayList<Proveedor>>() {
            @Override
            public void onResponse(Call<ArrayList<Proveedor>> call, Response<ArrayList<Proveedor>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Proveedor> proveedorNom = response.body();
                    List<String> list = new ArrayList<String>();
                    for (Proveedor p : proveedorNom) {
                        list.add(p.getNombreEmp());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddVehProv.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Proveedor>> call, Throwable t) {
            }
        });
    }

    private String uploadImage() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);

        return encodedImage;
    }

    private void validarDatos() {
        Pattern patternPlaca = Pattern.compile("^[A-Z0-9]{3}-[A-Z0-9]{3}$");
        Matcher matcherPlaca = patternPlaca.matcher(binding.etAddVehPla.getText().toString());

        if (binding.etAddVehPla.getText().toString().isEmpty()) {
            binding.etAddVehPla.setError("Este campo no puede estar vacio.");
        } else if (!matcherPlaca.find()) {
            binding.etAddVehPla.setError("Ingrese una placa valida.");
        } else if (binding.etAddVehPre.getText().toString().isEmpty()) {
            binding.etAddVehPre.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddVehAnio.getText().toString().isEmpty()) {
            binding.etAddVehAnio.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddVehAnio.getText().toString().length() != 4) {
            binding.etAddVehAnio.setError("Este campo debe contener 4 caracteres.");
        } else if (binding.ivAddVehImg.getDrawable() == null) {
            Toast.makeText(getContext().getApplicationContext(), "Debe elegir una imagen.", Toast.LENGTH_SHORT).show();
        }
    }

}