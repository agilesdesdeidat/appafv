package com.example.appafv.Fragments.Proveedor;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Proveedor;
import com.example.appafv.databinding.ActivityDetalleProveedorBinding;
import java.util.UUID;

public class DetalleProveedor extends AppCompatActivity {

    private ActivityDetalleProveedorBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetalleProveedorBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("detalleProveedor")) {
            Proveedor objProveedor = this.getIntent().getParcelableExtra("detalleProveedor");
            binding.tvDetProvId.setText(objProveedor.getProveedorId().toString());
            binding.tvDetProvEmp.setText(objProveedor.getNombreEmp());
            binding.tvDetProvRuc.setText(objProveedor.getRuc());
            binding.tvDetProvTel.setText(objProveedor.getTelefono());
            binding.tvDetProvDir.setText(objProveedor.getDireccion());
            binding.tvDetProvImg.setText(objProveedor.getImgp());
            Glide.with(this)
                    .load(objProveedor.getImgp())
                    .signature(new ObjectKey(UUID.randomUUID().toString()))
                    .into(binding.ivDetProvImg);
        }

        binding.btnRegresarDetProv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetalleProveedor.this, MenuPrincipalActivity.class);
                startActivity(intent);
                finish();
            }
        });

        binding.btnEditarProv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Proveedor objEditProv = new Proveedor();
                objEditProv.setProveedorId(Integer.parseInt(binding.tvDetProvId.getText().toString()));
                objEditProv.setNombreEmp(binding.tvDetProvEmp.getText().toString());
                objEditProv.setRuc(binding.tvDetProvRuc.getText().toString());
                objEditProv.setTelefono(binding.tvDetProvTel.getText().toString());
                objEditProv.setDireccion(binding.tvDetProvDir.getText().toString());
                objEditProv.setImgp(binding.tvDetProvImg.getText().toString());
                Intent editarp = new Intent(getApplicationContext(), EditarProveedor.class);
                editarp.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                editarp.putExtra("editarProveedor", objEditProv);
                getApplicationContext().startActivity(editarp);
            }
        });
    }

}