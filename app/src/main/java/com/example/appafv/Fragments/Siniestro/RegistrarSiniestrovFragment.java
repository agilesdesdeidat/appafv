package com.example.appafv.Fragments.Siniestro;

import androidx.lifecycle.ViewModelProvider;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Empleado;
import com.example.appafv.Model.MotivoSin;
import com.example.appafv.Model.SiniestroV;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.Utils.EmpleadoApiAdapter;
import com.example.appafv.Utils.MotivoSinApiAdapter;
import com.example.appafv.Utils.SiniestroApiAdapter;
import com.example.appafv.Utils.VehiculoApiAdapter;
import com.example.appafv.databinding.RegistrarSiniestrovFragmentBinding;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrarSiniestrovFragment extends Fragment {

    private RegistrarSiniestrovViewModel mViewModel;
    private RegistrarSiniestrovFragmentBinding binding;

    public static RegistrarSiniestrovFragment newInstance() {
        return new RegistrarSiniestrovFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = RegistrarSiniestrovFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(RegistrarSiniestrovViewModel.class);
        // TODO: Use the ViewModel

        obtenerVehiculoDisponible();
        obtenerConductorVehiculo();
        obtenerTipoSin();
        obtenerMotivoSin();

        binding.btnAddSinFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dia, mes, ano;
                final Calendar c = Calendar.getInstance();
                dia = c.get(Calendar.DAY_OF_MONTH);
                mes = c.get(Calendar.MONTH);
                ano = c.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        binding.etAddSinFecha.setText(year + "-" + (month+1) + "-" + dayOfMonth);
                    }
                }, dia, mes, ano);
                datePickerDialog.show();
            }
        });

        binding.ibAddSinImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.etAddSinUrl.getText().toString().isEmpty()) {
                    Toast.makeText(getContext().getApplicationContext(), "No ha colocado la URL.", Toast.LENGTH_SHORT).show();
                } else {
                    Glide.with(getContext().getApplicationContext())
                            .load(binding.etAddSinUrl.getText().toString())
                            .into(binding.ivAddSinImg);
                }
            }
        });

        binding.btnAddSin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etAddSinFecha.getText().toString().isEmpty() || binding.etAddSinUrl.getText().toString().isEmpty() ||
                        binding.etAddSinDetails.getText().toString().isEmpty() || binding.ivAddSinImg.getDrawable() == null) {
                    validarDatos();
                } else {
                    SiniestroV sv = new SiniestroV();
                    sv.setVehiculo_vehiculoId(binding.spAddSinVeh.getSelectedItem().toString());
                    sv.setEmpleado_empleadoId(binding.spAddSinEmp.getSelectedItem().toString());
                    sv.setFecha(binding.etAddSinFecha.getText().toString());
                    sv.setTipo(binding.spAddSinTipo.getSelectedItem().toString());
                    sv.setMotivo_Siniestro_motivosId(binding.spAddSinMotivo.getSelectedItem().toString());
                    sv.setImgs(binding.etAddSinUrl.getText().toString());
                    sv.setDetalle(binding.etAddSinDetails.getText().toString());
                    addSinVehiculo(sv);
                }
            }
        });
    }

    private void addSinVehiculo(SiniestroV sv) {
        Call<SiniestroV> call = SiniestroApiAdapter.getApiService().addSiniestroV(sv);
        call.enqueue(new Callback<SiniestroV>() {
            @Override
            public void onResponse(Call<SiniestroV> call, Response<SiniestroV> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getContext().getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getContext().getApplicationContext(), "Siniestro Vehículo Registado con Exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SiniestroV> call, Throwable t) {
            }
        });
    }

    private void obtenerMotivoSin() {
        Call<ArrayList<MotivoSin>> call = MotivoSinApiAdapter.getApiService().getMotivoSin();
        call.enqueue(new Callback<ArrayList<MotivoSin>>() {
            @Override
            public void onResponse(Call<ArrayList<MotivoSin>> call, Response<ArrayList<MotivoSin>> response) {
                ArrayList<MotivoSin> listMot = response.body();
                List<String> list = new ArrayList<>();
                for (MotivoSin ms : listMot) {
                    list.add(ms.getDescripcion());
                }
                ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                binding.spAddSinMotivo.setAdapter(spArrayAdapter);
            }

            @Override
            public void onFailure(Call<ArrayList<MotivoSin>> call, Throwable t) {
            }
        });
    }

    private void obtenerTipoSin() {
        List<String> listTipo = new ArrayList<>();
        listTipo.add("Leve");
        listTipo.add("Medio");
        listTipo.add("Grave");
        ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, listTipo);
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spAddSinTipo.setAdapter(spArrayAdapter);
    }

    private void obtenerConductorVehiculo() {
        Call<ArrayList<Empleado>> call = EmpleadoApiAdapter.getApiService().getEmpleadoConductor();
        call.enqueue(new Callback<ArrayList<Empleado>>() {
            @Override
            public void onResponse(Call<ArrayList<Empleado>> call, Response<ArrayList<Empleado>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Empleado> empCon = response.body();
                    List<String> list = new ArrayList<>();
                    for (Empleado e : empCon) {
                        list.add(e.getNombrec());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddSinEmp.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Empleado>> call, Throwable t) {
            }
        });
    }

    private void obtenerVehiculoDisponible() {
        Call<ArrayList<Vehiculo>> call = VehiculoApiAdapter.getApiService().getVehiculoDisponible();
        call.enqueue(new Callback<ArrayList<Vehiculo>>() {
            @Override
            public void onResponse(Call<ArrayList<Vehiculo>> call, Response<ArrayList<Vehiculo>> response) {
                if(response.isSuccessful()) {
                    ArrayList<Vehiculo> vehiculoPlaca = response.body();
                    List<String> list = new ArrayList<>();
                    for (Vehiculo v : vehiculoPlaca) {
                        list.add(v.getPlaca());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddSinVeh.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Vehiculo>> call, Throwable t) {
            }
        });
    }

    private void validarDatos() {
        if (binding.etAddSinFecha.getText().toString().isEmpty()) {
            binding.etAddSinFecha.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddSinUrl.getText().toString().isEmpty()) {
            binding.etAddSinUrl.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddSinDetails.getText().toString().isEmpty()) {
            binding.etAddSinDetails.setError("Este campo no puede estar vacio.");
        } else if (binding.ivAddSinImg.getDrawable() == null) {
            Toast.makeText(getContext().getApplicationContext(), "Debe visualizar la URL.", Toast.LENGTH_SHORT).show();
        }
    }

}