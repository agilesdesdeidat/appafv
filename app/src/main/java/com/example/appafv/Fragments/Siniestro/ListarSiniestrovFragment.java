package com.example.appafv.Fragments.Siniestro;

import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import com.example.appafv.Adapter.SiniestroAdapter;
import com.example.appafv.Model.SiniestroV;
import com.example.appafv.Utils.SiniestroApiAdapter;
import com.example.appafv.databinding.ListarSiniestrovFragmentBinding;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListarSiniestrovFragment extends Fragment {

    private ListarSiniestrovViewModel mViewModel;
    private ListarSiniestrovFragmentBinding binding;

    ArrayList<SiniestroV> sinList;

    public static ListarSiniestrovFragment newInstance() {
        return new ListarSiniestrovFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = ListarSiniestrovFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ListarSiniestrovViewModel.class);
        // TODO: Use the ViewModel

        binding.rvSiniestroVehiculo.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        obtenerSiniestrosVehiculo();
        binding.svBuscarSiniestro.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<SiniestroV> clist = new ArrayList<>();
                for (SiniestroV obj : sinList) {
                    if (obj.getVehiculo_vehiculoId().toLowerCase().contains(newText.toLowerCase())) {
                        clist.add(obj);
                    }
                }
                binding.rvSiniestroVehiculo.setAdapter(new SiniestroAdapter(getContext().getApplicationContext(), clist));
                return true;
            }
        });
    }

    private void obtenerSiniestrosVehiculo() {
        Call<ArrayList<SiniestroV>> call = SiniestroApiAdapter.getApiService().getSiniestros();
        call.enqueue(new Callback<ArrayList<SiniestroV>>() {
            @Override
            public void onResponse(Call<ArrayList<SiniestroV>> call, Response<ArrayList<SiniestroV>> response) {
                sinList = response.body();
                binding.rvSiniestroVehiculo.setAdapter(new SiniestroAdapter(getContext().getApplicationContext(), sinList));
            }

            @Override
            public void onFailure(Call<ArrayList<SiniestroV>> call, Throwable t) {
            }
        });
    }

}