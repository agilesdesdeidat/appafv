package com.example.appafv.Fragments.Mantenimiento;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Mantenimiento;
import com.example.appafv.Utils.MantApiAdapter;
import com.example.appafv.databinding.ActivityDetalleMantBinding;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetalleMant extends AppCompatActivity {

    private ActivityDetalleMantBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetalleMantBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("detalleMant")) {
            Mantenimiento objMant = this.getIntent().getParcelableExtra("detalleMant");
            binding.tvDetMantId.setText(objMant.getMantenimientoId().toString());
            binding.tvDetMantVeh.setText(objMant.getVehiculo_vehiculoId());
            binding.tvDetMantEmp.setText(objMant.getEmpleado_empleadoId());
            binding.tvDetMantFecha.setText(objMant.getFecha());
            binding.tvDetMantTipo.setText(objMant.getTipo_Mant_tipomId());
            binding.tvDetMantMotivo.setText(objMant.getMotivo_Mant_motivomId());
            binding.tvDetMantPrecio.setText(objMant.getPrecio().toString());
            binding.tvDetMantImg.setText(objMant.getImgm());
            binding.tvDetMantObs.setText(objMant.getObservacion());
            binding.tvDetMantEst.setText(objMant.getEstado_Mant_estadomId().toString());
            Glide.with(this)
                    .load(objMant.getImgm())
                    .signature(new ObjectKey(UUID.randomUUID().toString()))
                    .into(binding.ivDetMantImg);
        }

        if (binding.tvDetMantEst.getText().toString().equals("2")) {
            binding.btnDetMantFin.setVisibility(View.INVISIBLE);
        }

        binding.btnDetMantDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(DetalleMant.this);
                dialog.setTitle("ALERTA DE ELIMINACION");
                dialog.setMessage("Esta a punto de eliminar este mantenimiento. Quieres continuar?");
                dialog.setCancelable(false);
                dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(DetalleMant.this, "Operacion cancelada", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Call<Mantenimiento> call = MantApiAdapter.getApiService().delMant(Integer.parseInt(binding.tvDetMantId.getText().toString()));
                        call.enqueue(new Callback<Mantenimiento>() {
                            @Override
                            public void onResponse(Call<Mantenimiento> call, Response<Mantenimiento> response) {
                                if (response.isSuccessful()) {
                                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(), "Mantenimiento eliminado con exito!", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Mantenimiento> call, Throwable t) {
                            }
                        });
                    }
                });
                AlertDialog dialog1 = dialog.create();
                dialog1.show();
            }
        });

        binding.btnDetMantFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(DetalleMant.this);
                dialog.setTitle("ALERTA DE CAMBIO DE ESTADO");
                dialog.setMessage("Esta a punto de terminar este mantenimiento, es decir, que se realizo correctamente el mantenimiento. Quieres continuar?");
                dialog.setCancelable(false);
                dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(DetalleMant.this, "Operacion cancelada", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Mantenimiento mv = new Mantenimiento();
                        Integer id = Integer.parseInt(binding.tvDetMantId.getText().toString());
                        mv.setVehiculo_vehiculoId(binding.tvDetMantVeh.getText().toString());
                        mv.setEmpleado_empleadoId(binding.tvDetMantEmp.getText().toString());
                        mv.setFecha(binding.tvDetMantFecha.getText().toString());
                        mv.setTipo_Mant_tipomId(binding.tvDetMantTipo.getText().toString());
                        mv.setMotivo_Mant_motivomId(binding.tvDetMantMotivo.getText().toString());
                        mv.setPrecio(Double.parseDouble(binding.tvDetMantPrecio.getText().toString()));
                        mv.setImgm(binding.tvDetMantImg.getText().toString());
                        mv.setObservacion(binding.tvDetMantObs.getText().toString());
                        if (binding.tvDetMantEst.getText().toString().equals("1")) {
                            mv.setEstado_Mant_estadomId(2);
                        } else {
                            mv.setEstado_Mant_estadomId(1);
                        }
                        Call<Mantenimiento> call = MantApiAdapter.getApiService().updMant(id, mv);
                        call.enqueue(new Callback<Mantenimiento>() {
                            @Override
                            public void onResponse(Call<Mantenimiento> call, Response<Mantenimiento> response) {
                                if(response.isSuccessful()) {
                                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(), "Mantenimiento terminado con exito!", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Mantenimiento> call, Throwable t) {
                            }
                        });
                    }
                });
                AlertDialog dialog1 = dialog.create();
                dialog1.show();
            }
        });
    }

}