package com.example.appafv.Fragments.Siniestro;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Empleado;
import com.example.appafv.Model.MotivoSin;
import com.example.appafv.Model.Siniestro;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.Utils.EmpleadoApiAdapter;
import com.example.appafv.Utils.MotivoSinApiAdapter;
import com.example.appafv.Utils.SiniestroApiAdapter;
import com.example.appafv.Utils.VehiculoApiAdapter;
import com.example.appafv.databinding.ActivityEditarSiniestroBinding;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarSiniestro extends AppCompatActivity {

    private ActivityEditarSiniestroBinding binding;
    private int IMG_REQUEST = 21;
    private Bitmap bitmap;
    private int editImage = 0;

    String nomVeh;
    String nomEmp;
    String nomTipo;
    String nomMotivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEditarSiniestroBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("editSin")) {
            Siniestro objSin = this.getIntent().getParcelableExtra("editSin");
            binding.tvEditSinId.setText(objSin.getSiniestroId().toString());
            nomVeh = objSin.getVehiculo_vehiculoId();
            nomEmp = objSin.getEmpleado_empleadoId();
            binding.etEditSinFecha.setText(objSin.getFecha());
            nomTipo = objSin.getTipo();
            nomMotivo = objSin.getMotivo_Siniestro_motivosId();
            binding.etEditSinUrl.setText(objSin.getImgs());
            binding.etEditSinDetalle.setText(objSin.getDetalle());
            Glide.with(this)
                    .load(objSin.getImgs())
                    .signature(new ObjectKey(UUID.randomUUID().toString()))
                    .into(binding.ivEditSinImg);
        }

        obtenerPlacaVehiculo();
        obtenerNomConductor();
        obtenerDesTipo();
        obtenerDescMotivo();

        binding.btnEditSinFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dia, mes, ano;
                final Calendar c = Calendar.getInstance();
                dia = c.get(Calendar.DAY_OF_MONTH);
                mes = c.get(Calendar.MONTH);
                ano = c.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(EditarSiniestro.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        binding.etEditSinFecha.setText(String.format("%02d-%02d-%02d", ano, mes+1, dia));
                    }
                }, dia, mes, ano);
                datePickerDialog.show();
            }
        });

        binding.ibEditCargaUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editImage = 1;

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMG_REQUEST);
            }
        });

        binding.btnEditSinv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etEditSinFecha.getText().toString().isEmpty() || binding.ivEditSinImg.getDrawable() == null ||
                        binding.etEditSinDetalle.getText().toString().isEmpty()) {
                    validarDatos();
                } else {
                    Siniestro sv = new Siniestro();
                    Integer id = Integer.parseInt(binding.tvEditSinId.getText().toString());
                    sv.setVehiculo_vehiculoId(binding.spEditSinVeh.getSelectedItem().toString());
                    sv.setEmpleado_empleadoId(binding.spEditSinEmp.getSelectedItem().toString());
                    sv.setFecha(binding.etEditSinFecha.getText().toString());
                    sv.setTipo(binding.spEditSinTipo.getSelectedItem().toString());
                    sv.setMotivo_Siniestro_motivosId(binding.spEditSinMotivo.getSelectedItem().toString());
                    sv.setDetalle(binding.etEditSinDetalle.getText().toString());
                    
                    if (editImage == 1) {
                        sv.setImgs(uploadImage());
                    } else {
                        sv.setImgs(binding.etEditSinUrl.getText().toString());
                    }

                    updSiniestro(id, sv);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                binding.ivEditSinImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void updSiniestro(Integer id, Siniestro sv) {
        Call<Siniestro> call = SiniestroApiAdapter.getApiService().updSiniestro(id, sv);
        call.enqueue(new Callback<Siniestro>() {
            @Override
            public void onResponse(Call<Siniestro> call, Response<Siniestro> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Siniestro Vehículo actualizado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Siniestro> call, Throwable t) {
            }
        });
    }

    private void obtenerDescMotivo() {
        Call<ArrayList<MotivoSin>> call = MotivoSinApiAdapter.getApiService().getMotivoSin();
        call.enqueue(new Callback<ArrayList<MotivoSin>>() {
            @Override
            public void onResponse(Call<ArrayList<MotivoSin>> call, Response<ArrayList<MotivoSin>> response) {
                ArrayList<MotivoSin> listMot = response.body();
                List<String> list = new ArrayList<>();
                list.add(nomMotivo);
                for (MotivoSin ms : listMot) {
                    list.add(ms.getDescripcion());
                }
                ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                binding.spEditSinMotivo.setAdapter(spArrayAdapter);
            }

            @Override
            public void onFailure(Call<ArrayList<MotivoSin>> call, Throwable t) {
            }
        });
    }

    private void obtenerDesTipo() {
        List<String> listTipo = new ArrayList<>();
        listTipo.add(nomTipo);
        listTipo.add("Leve");
        listTipo.add("Medio");
        listTipo.add("Grave");
        ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, listTipo);
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spEditSinTipo.setAdapter(spArrayAdapter);
    }

    private void obtenerNomConductor() {
        Call<ArrayList<Empleado>> call = EmpleadoApiAdapter.getApiService().getEmpleadoConductor();
        call.enqueue(new Callback<ArrayList<Empleado>>() {
            @Override
            public void onResponse(Call<ArrayList<Empleado>> call, Response<ArrayList<Empleado>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Empleado> empCon = response.body();
                    List<String> list = new ArrayList<>();
                    list.add(nomEmp);
                    for (Empleado e : empCon) {
                        list.add(e.getNombrec());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spEditSinEmp.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Empleado>> call, Throwable t) {
            }
        });
    }

    private void obtenerPlacaVehiculo() {
        Call<ArrayList<Vehiculo>> call = VehiculoApiAdapter.getApiService().getVehiculoDisponible();
        call.enqueue(new Callback<ArrayList<Vehiculo>>() {
            @Override
            public void onResponse(Call<ArrayList<Vehiculo>> call, Response<ArrayList<Vehiculo>> response) {
                if(response.isSuccessful()) {
                    ArrayList<Vehiculo> vehiculoPlaca = response.body();
                    List<String> list = new ArrayList<>();
                    list.add(nomVeh);
                    for (Vehiculo v : vehiculoPlaca) {
                        list.add(v.getPlaca());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spEditSinVeh.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Vehiculo>> call, Throwable t) {
            }
        });
    }

    private String uploadImage() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);

        return encodedImage;
    }

    private void validarDatos() {
        if (binding.etEditSinFecha.getText().toString().isEmpty()) {
            binding.etEditSinFecha.setError("Este campo no puede estar vacio.");
        } else if (binding.ivEditSinImg.getDrawable() == null) {
            Toast.makeText(getApplicationContext(), "Debe elegir una imagen.", Toast.LENGTH_SHORT).show();
        } else if (binding.etEditSinDetalle.getText().toString().isEmpty()) {
            binding.etEditSinDetalle.setError("Este campo no puede estar vacio.");
        }
    }

}