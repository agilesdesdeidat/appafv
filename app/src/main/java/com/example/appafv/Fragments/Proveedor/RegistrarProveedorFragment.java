package com.example.appafv.Fragments.Proveedor;

import androidx.lifecycle.ViewModelProvider;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Proveedor;
import com.example.appafv.Utils.ProveedorApiAdapter;
import com.example.appafv.databinding.RegistrarProveedorFragmentBinding;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrarProveedorFragment extends Fragment {

    private RegistrarProveedorViewModel mViewModel;
    private RegistrarProveedorFragmentBinding binding;
    private int IMG_REQUEST = 21;
    private Bitmap bitmap;

    public static RegistrarProveedorFragment newInstance() {
        return new RegistrarProveedorFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = RegistrarProveedorFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(RegistrarProveedorViewModel.class);

        binding.ibAddCargaUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMG_REQUEST);
            }
        });

        binding.btnProvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etProvEmp.getText().toString().isEmpty() || binding.etProvRuc.getText().toString().isEmpty() ||
                        binding.etProvRuc.getText().toString().length() != 11 || binding.etProvTel.getText().toString().isEmpty() ||
                        binding.etProvTel.getText().toString().length() != 7 || binding.etProvDir.getText().toString().isEmpty()) {
                    validarDatos();
                } else {
                    Proveedor p = new Proveedor();
                    p.setNombreEmp(binding.etProvEmp.getText().toString());
                    p.setRuc(binding.etProvRuc.getText().toString());
                    p.setTelefono(binding.etProvTel.getText().toString());
                    p.setDireccion(binding.etProvDir.getText().toString());
                    if (binding.ivProvAddImg.getDrawable() == null) p.setImgp("");
                    else p.setImgp(uploadImage());
                    addProveedor(p);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);
                binding.ivProvAddImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addProveedor(Proveedor p) {
        Call<Proveedor> call = ProveedorApiAdapter.getApiService().addProveedor(p);
        call.enqueue(new Callback<Proveedor>() {
            @Override
            public void onResponse(Call<Proveedor> call, Response<Proveedor> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getContext().getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getContext().getApplicationContext(), "Proveedor registrado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Proveedor> call, Throwable t) {
            }
        });
    }

    private String uploadImage() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);

        return encodedImage;
    }

    private void validarDatos() {
        if (binding.etProvEmp.getText().toString().isEmpty()) {
            binding.etProvEmp.setError("Este campo no puede estar vacio.");
        } else if (binding.etProvRuc.getText().toString().isEmpty()) {
            binding.etProvRuc.setError("Este campo no puede estar vacio.");
        } else if (binding.etProvRuc.getText().toString().length() != 11) {
            binding.etProvRuc.setError("Este campo debe contener 11 caracteres.");
        } else if (binding.etProvTel.getText().toString().isEmpty()) {
            binding.etProvTel.setError("Este campo no puede estar vacio.");
        } else if (binding.etProvTel.getText().toString().length() != 7) {
            binding.etProvTel.setError("Este campo debe contener 7 caracteres.");
        } else if (binding.etProvDir.getText().toString().isEmpty()) {
            binding.etProvDir.setError("Este campo no puede estar vacio.");
        }
    }

}