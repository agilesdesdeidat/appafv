package com.example.appafv.Fragments.Proveedor;

import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import com.example.appafv.Adapter.ProveedorAdapter;
import com.example.appafv.Model.Proveedor;
import com.example.appafv.Utils.ProveedorApiAdapter;
import com.example.appafv.databinding.ListaProveedorFragmentBinding;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaProveedorFragment extends Fragment {

    private ListaProveedorViewModel mViewModel;
    private ListaProveedorFragmentBinding binding;

    ArrayList<Proveedor> proveedorList;

    public static ListaProveedorFragment newInstance() {
        return new ListaProveedorFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = ListaProveedorFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ListaProveedorViewModel.class);
        // TODO: Use the ViewModel

        binding.rvProveedores.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        obtenerProveedores();
        proveedorList = new ArrayList<>();

        binding.svBuscarProveedor.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<Proveedor> mlista = new ArrayList<>();
                for(Proveedor obj : proveedorList) {
                    if (obj.getNombreEmp().toLowerCase().contains(newText.toLowerCase())) {
                        mlista.add(obj);
                    }
                }
                binding.rvProveedores.setAdapter(new ProveedorAdapter(getContext().getApplicationContext(), mlista));
                return true;
            }
        });
    }

    private void obtenerProveedores() {
        Call<ArrayList<Proveedor>> call = ProveedorApiAdapter.getApiService().getProveedores();
        call.enqueue(new Callback<ArrayList<Proveedor>>() {
            @Override
            public void onResponse(Call<ArrayList<Proveedor>> call, Response<ArrayList<Proveedor>> response) {
                proveedorList = response.body();
                binding.rvProveedores.setAdapter(new ProveedorAdapter(getContext().getApplicationContext(), proveedorList));
            }

            @Override
            public void onFailure(Call<ArrayList<Proveedor>> call, Throwable t) {
            }
        });
    }

}