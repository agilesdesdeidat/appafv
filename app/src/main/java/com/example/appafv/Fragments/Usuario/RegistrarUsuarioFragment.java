package com.example.appafv.Fragments.Usuario;

import androidx.lifecycle.ViewModelProvider;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Usuario;
import com.example.appafv.Utils.UsuarioApiAdapter;
import com.example.appafv.databinding.RegistrarUsuarioFragmentBinding;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrarUsuarioFragment extends Fragment {

    private RegistrarUsuarioViewModel mViewModel;
    private RegistrarUsuarioFragmentBinding binding;
    private int IMG_REQUEST = 21;
    private Bitmap bitmap;

    public static RegistrarUsuarioFragment newInstance() {
        return new RegistrarUsuarioFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = RegistrarUsuarioFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(RegistrarUsuarioViewModel.class);
        // TODO: Use the ViewModel

        obtenerRoles();

        binding.ibAddCargaUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMG_REQUEST);
            }
        });

        binding.btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etAddUserNom.getText().toString().isEmpty() || binding.etAddUserU.getText().toString().isEmpty() ||
                        binding.etAddUserPass.getText().toString().isEmpty() || binding.etAddUserEmail.getText().toString().isEmpty() ||
                        binding.ivAddUserImg.getDrawable() == null) {
                    validarDatos();
                } else {
                    Usuario user = new Usuario();
                    user.setNombrec(binding.etAddUserNom.getText().toString());
                    user.setUsername(binding.etAddUserU.getText().toString());
                    user.setPassword(binding.etAddUserPass.getText().toString());
                    user.setRol(binding.spRoles.getSelectedItem().toString());
                    user.setCorreo(binding.etAddUserEmail.getText().toString());
                    user.setImgu(uploadImage());
                    user.setEstadou("Activo");
                    addUsuario(user);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);
                binding.ivAddUserImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addUsuario(Usuario user) {
        Call<Usuario> call = UsuarioApiAdapter.getApiService().addUsuario(user);
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                Intent intent = new Intent(getContext().getApplicationContext(), MenuPrincipalActivity.class);
                startActivity(intent);
                Toast.makeText(getContext().getApplicationContext(), "Usuario registrado con exito!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
            }
        });
    }

    private void obtenerRoles() {
        List<String> listr = new ArrayList<>();
        listr.add("Administrador");
        listr.add("Almacenero");
        listr.add("Controlador");
        listr.add("Mecanico");
        ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, listr);
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spRoles.setAdapter(spArrayAdapter);
    }

    private String uploadImage() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);

        return encodedImage;
    }

    private void validarDatos() {
        Pattern patternEmail = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcherEmail = patternEmail.matcher(binding.etAddUserEmail.getText().toString());

        if (binding.etAddUserNom.getText().toString().isEmpty()) {
            binding.etAddUserNom.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddUserU.getText().toString().isEmpty()) {
            binding.etAddUserU.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddUserU.getText().toString().length() < 5) {
            binding.etAddUserU.setError("Este campo debe contener minimo 5 caracteres.");
        } else if (binding.etAddUserPass.getText().toString().isEmpty()) {
            binding.etAddUserPass.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddUserPass.getText().toString().length() < 8 ) {
            binding.etAddUserPass.setError("Este campo debe contener minimo 8 caracteres.");
        } else if (binding.etAddUserEmail.getText().toString().isEmpty()) {
            binding.etAddUserEmail.setError("Este campo no puede estar vacio.");
        } else if (!matcherEmail.find()) {
            binding.etAddUserEmail.setError("Ingrese un email valido.");
        } else if (binding.ivAddUserImg.getDrawable() == null) {
            Toast.makeText(getContext().getApplicationContext(), "Debe elegir una imagen.", Toast.LENGTH_SHORT).show();
        }
    }

}