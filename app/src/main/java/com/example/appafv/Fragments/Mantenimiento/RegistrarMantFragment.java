package com.example.appafv.Fragments.Mantenimiento;

import androidx.lifecycle.ViewModelProvider;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Empleado;
import com.example.appafv.Model.Mantenimiento;
import com.example.appafv.Model.MotivoMant;
import com.example.appafv.Model.TipoMant;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.Utils.EmpleadoApiAdapter;
import com.example.appafv.Utils.MantApiAdapter;
import com.example.appafv.Utils.MotivoMantApiAdapter;
import com.example.appafv.Utils.TipoMantApiAdapter;
import com.example.appafv.Utils.VehiculoApiAdapter;
import com.example.appafv.databinding.RegistrarMantFragmentBinding;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrarMantFragment extends Fragment {

    private RegistrarMantViewModel mViewModel;
    private RegistrarMantFragmentBinding binding;
    private int IMG_REQUEST = 21;
    private Bitmap bitmap;

    public static RegistrarMantFragment newInstance() {
        return new RegistrarMantFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = RegistrarMantFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(RegistrarMantViewModel.class);
        // TODO: Use the ViewModel

        obtenerVehiculoDisponible();
        obtenerEmpleadoMecanico();
        obtenerTipoMantV();
        obtenerMotivoMantV();

        binding.btnMantFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dia, mes, ano;
                final Calendar c = Calendar.getInstance();
                dia = c.get(Calendar.DAY_OF_MONTH);
                mes = c.get(Calendar.MONTH);
                ano = c.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        binding.etAddMantFecha.setText(year + "-" + (month+1) + "-" + dayOfMonth);
                    }
                }, dia, mes, ano);
                datePickerDialog.show();
            }
        });

        binding.ibAddCargaUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMG_REQUEST);
            }
        });

        binding.btnRegistrarMant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etAddMantFecha.getText().toString().isEmpty() || binding.etAddMantPre.getText().toString().isEmpty() ||
                        binding.etAddMantObs.getText().toString().isEmpty() || binding.ivAddMant.getDrawable() == null) {
                    validarDatos();
                } else {
                    Mantenimiento mv = new Mantenimiento();
                    mv.setVehiculo_vehiculoId(binding.spAddMantVeh.getSelectedItem().toString());
                    mv.setEmpleado_empleadoId(binding.spAddMantEmp.getSelectedItem().toString());
                    mv.setFecha(binding.etAddMantFecha.getText().toString());
                    mv.setTipo_Mant_tipomId(binding.spAddMantTipo.getSelectedItem().toString());
                    mv.setMotivo_Mant_motivomId(binding.spAddMantMotivo.getSelectedItem().toString());
                    mv.setPrecio(Double.parseDouble(binding.etAddMantPre.getText().toString()));
                    mv.setImgm(uploadImage());
                    mv.setObservacion(binding.etAddMantObs.getText().toString());
                    mv.setEstado_Mant_estadomId(1);
                    addMantenimiento(mv);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);
                binding.ivAddMant.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addMantenimiento(Mantenimiento mv) {
        Call<Mantenimiento> call = MantApiAdapter.getApiService().addMant(mv);
        call.enqueue(new Callback<Mantenimiento>() {
            @Override
            public void onResponse(Call<Mantenimiento> call, Response<Mantenimiento> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getContext().getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getContext().getApplicationContext(), "Mant. Vehiculo registrado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Mantenimiento> call, Throwable t) {
            }
        });
    }

    private void obtenerMotivoMantV() {
        Call<ArrayList<MotivoMant>> call = MotivoMantApiAdapter.getApiService().getMotivoMant();
        call.enqueue(new Callback<ArrayList<MotivoMant>>() {
            @Override
            public void onResponse(Call<ArrayList<MotivoMant>> call, Response<ArrayList<MotivoMant>> response) {
                if (response.isSuccessful()) {
                    ArrayList<MotivoMant> motivoDesc = response.body();
                    List<String> list = new ArrayList<>();
                    for (MotivoMant mm : motivoDesc) {
                        list.add(mm.getDescripcion());
                    }
                    ArrayAdapter<String> spArrayAdapter= new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddMantMotivo.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MotivoMant>> call, Throwable t) {
            }
        });
    }

    private void obtenerTipoMantV() {
        Call<ArrayList<TipoMant>> call = TipoMantApiAdapter.getApiService().getTipoMant();
        call.enqueue(new Callback<ArrayList<TipoMant>>() {
            @Override
            public void onResponse(Call<ArrayList<TipoMant>> call, Response<ArrayList<TipoMant>> response) {
                if (response.isSuccessful()) {
                    ArrayList<TipoMant> tipoDesc = response.body();
                    List<String> list = new ArrayList<>();
                    for (TipoMant tm : tipoDesc) {
                        list.add(tm.getDescripcion());
                    }
                    ArrayAdapter<String> spArrayAdapter= new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddMantTipo.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TipoMant>> call, Throwable t) {
            }
        });
    }

    private void obtenerEmpleadoMecanico() {
        Call<ArrayList<Empleado>> call = EmpleadoApiAdapter.getApiService().getEmpleadoMecanico();
        call.enqueue(new Callback<ArrayList<Empleado>>() {
            @Override
            public void onResponse(Call<ArrayList<Empleado>> call, Response<ArrayList<Empleado>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Empleado> empleadoMec = response.body();
                    List<String> list = new ArrayList<>();
                    for (Empleado e : empleadoMec) {
                        list.add(e.getNombrec());
                    }
                    ArrayAdapter<String> spArrayAdapter= new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddMantEmp.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Empleado>> call, Throwable t) {
            }
        });
    }

    private void obtenerVehiculoDisponible() {
        Call<ArrayList<Vehiculo>> call = VehiculoApiAdapter.getApiService().getVehiculos();
        call.enqueue(new Callback<ArrayList<Vehiculo>>() {
            @Override
            public void onResponse(Call<ArrayList<Vehiculo>> call, Response<ArrayList<Vehiculo>> response) {
                if(response.isSuccessful()) {
                    ArrayList<Vehiculo> vehiculoPlaca = response.body();
                    List<String> list = new ArrayList<>();
                    for (Vehiculo v : vehiculoPlaca) {
                        list.add(v.getPlaca());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddMantVeh.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Vehiculo>> call, Throwable t) {
            }
        });
    }

    private String uploadImage() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);

        return encodedImage;
    }

    private void validarDatos() {
        if (binding.etAddMantFecha.getText().toString().isEmpty()) {
            binding.etAddMantFecha.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddMantPre.getText().toString().isEmpty()) {
            binding.etAddMantPre.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddMantObs.getText().toString().isEmpty()) {
            binding.etAddMantObs.setError("Este campo no puede estar vacio.");
        } else if (binding.ivAddMant.getDrawable() == null) {
            Toast.makeText(getContext().getApplicationContext(), "Debe elegir una imagen.", Toast.LENGTH_SHORT).show();
        }
    }

}