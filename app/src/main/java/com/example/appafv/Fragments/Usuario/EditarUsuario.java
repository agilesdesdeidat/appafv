package com.example.appafv.Fragments.Usuario;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Usuario;
import com.example.appafv.Utils.UsuarioApiAdapter;
import com.example.appafv.databinding.ActivityEditarUsuarioBinding;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarUsuario extends AppCompatActivity {

    private ActivityEditarUsuarioBinding binding;
    private int IMG_REQUEST = 21;
    private Bitmap bitmap;
    private int editImage = 0;

    String userRol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEditarUsuarioBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("editarUsuario")) {
            Usuario objUsuario = this.getIntent().getParcelableExtra("editarUsuario");
            binding.tvEditUserId.setText(objUsuario.getUsuarioId().toString());
            binding.etEditUserNom.setText(objUsuario.getNombrec());
            binding.etEditUserU.setText(objUsuario.getUsername());
            binding.etEditUserPass.setText(objUsuario.getPassword());
            userRol = objUsuario.getRol();
            binding.etEditUserEmail.setText(objUsuario.getCorreo());
            binding.etEditUserUrl.setText(objUsuario.getImgu());
            binding.tvEditUserEst.setText(objUsuario.getEstadou());
            Glide.with(this)
                    .load(objUsuario.getImgu()).apply(RequestOptions.circleCropTransform())
                    .signature(new ObjectKey(UUID.randomUUID().toString()))
                    .into(binding.ivEditUserImg);
        }

        obtenerRoles();

        binding.ibEditCargaUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editImage = 1;

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMG_REQUEST);
            }
        });

        binding.btnEditUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etEditUserNom.getText().toString().isEmpty() || binding.etEditUserU.getText().toString().isEmpty() ||
                        binding.etEditUserPass.getText().toString().isEmpty() || binding.etEditUserEmail.getText().toString().isEmpty() ||
                        binding.ivEditUserImg.getDrawable() == null) {
                    validarDatos();
                } else {
                    Usuario user = new Usuario();
                    Integer id = Integer.parseInt(binding.tvEditUserId.getText().toString());
                    user.setNombrec(binding.etEditUserNom.getText().toString());
                    user.setUsername(binding.etEditUserU.getText().toString());
                    user.setPassword(binding.etEditUserPass.getText().toString());
                    user.setCorreo(binding.etEditUserEmail.getText().toString());
                    user.setRol(binding.spEditUserRol.getSelectedItem().toString());
                    user.setEstadou(binding.tvEditUserEst.getText().toString());
                        
                    if (editImage == 1) {
                        user.setImgu(uploadImage());
                    } else {
                        user.setImgu(binding.etEditUserUrl.getText().toString());
                    }

                    updUsuario(id, user);
                }
            }
        });

        binding.btnCancelarEditUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditarUsuario.this, MenuPrincipalActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                binding.ivEditUserImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void updUsuario(Integer id, Usuario user) {
        Call<Usuario> call = UsuarioApiAdapter.getApiService().updUsuario(id, user);
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Usuario actualizado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
            }
        });
    }

    private void obtenerRoles() {
        List<String> listr = new ArrayList<>();
        listr.add(userRol);
        listr.add("Administrador");
        listr.add("Almacenero");
        listr.add("Controlador");
        listr.add("Mecanico");
        ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getApplication(), android.R.layout.simple_spinner_dropdown_item, listr);
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spEditUserRol.setAdapter(spArrayAdapter);
    }

    private String uploadImage() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);

        return encodedImage;
    }

    private void validarDatos() {
        Pattern patternEmail = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcherEmail = patternEmail.matcher(binding.etEditUserEmail.getText().toString());

        if (binding.etEditUserNom.getText().toString().isEmpty()) {
            binding.etEditUserNom.setError("Este campo no puede estar vacio.");
        } else if (binding.etEditUserU.getText().toString().isEmpty()) {
            binding.etEditUserU.setError("Este campo no puede estar vacio.");
        } else if (binding.etEditUserU.getText().toString().length() < 5) {
            binding.etEditUserU.setError("Este campo debe contener minimo 5 caracteres.");
        } else if (binding.etEditUserPass.getText().toString().isEmpty()) {
            binding.etEditUserPass.setError("Este campo no puede estar vacio.");
        } else if (binding.etEditUserPass.getText().toString().length() < 8) {
            binding.etEditUserPass.setError("Este campo debe contener minimo 8 caracteres.");
        } else if (binding.etEditUserEmail.getText().toString().isEmpty()) {
            binding.etEditUserEmail.setError("Este campo no puede estar vacio.");
        } else if (!matcherEmail.find()) {
            binding.etEditUserEmail.setError("Ingrese un email valido.");
        } else if (binding.ivEditUserImg.getDrawable() == null) {
            Toast.makeText(getApplicationContext(), "Debe elegir una imagen.", Toast.LENGTH_SHORT).show();
        }
    }

}