package com.example.appafv.Fragments.Usuario;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Usuario;
import com.example.appafv.Utils.UsuarioApiAdapter;
import com.example.appafv.databinding.ActivityDetalleUsuarioBinding;
import java.util.UUID;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetalleUsuario extends AppCompatActivity {

    private ActivityDetalleUsuarioBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetalleUsuarioBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("detalleUsuario")) {
            Usuario objUser = this.getIntent().getParcelableExtra("detalleUsuario");
            binding.tvDetUserId.setText(objUser.getUsuarioId().toString());
            binding.tvDetUserNom.setText(objUser.getNombrec());
            binding.tvDetUserU.setText(objUser.getUsername());
            binding.tvDetUserPass.setText(objUser.getPassword());
            binding.tvDetUserRol.setText(objUser.getRol());
            binding.tvDetUserEmail.setText(objUser.getCorreo());
            binding.tvDetUserUrl.setText(objUser.getImgu());
            binding.tvDetUserEst.setText(objUser.getEstadou());
            Glide.with(this)
                    .load(objUser.getImgu()).apply(RequestOptions.circleCropTransform())
                    .signature(new ObjectKey(UUID.randomUUID().toString()))
                    .into(binding.ivDetUserImg);
        }

        binding.btnRegresarUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetalleUsuario.this, MenuPrincipalActivity.class);
                startActivity(intent);
                finish();
            }
        });

        binding.btnEstUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario user = new Usuario();
                Integer id = Integer.parseInt(binding.tvDetUserId.getText().toString());
                user.setNombrec(binding.tvDetUserNom.getText().toString());
                user.setUsername(binding.tvDetUserU.getText().toString());
                user.setPassword(binding.tvDetUserPass.getText().toString());
                user.setCorreo(binding.tvDetUserEmail.getText().toString());
                user.setRol(binding.tvDetUserRol.getText().toString());
                user.setImgu(binding.tvDetUserUrl.getText().toString());
                if (binding.tvDetUserEst.getText().toString().equals("Activo")) {
                    user.setEstadou("Inactivo");
                } else {
                    user.setEstadou("Activo");
                }
                cambiarEstadoUsuario(id, user);
            }
        });

        binding.btnEditarUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario user = new Usuario();
                user.setUsuarioId(Integer.parseInt(binding.tvDetUserId.getText().toString()));
                user.setNombrec(binding.tvDetUserNom.getText().toString());
                user.setUsername(binding.tvDetUserU.getText().toString());
                user.setPassword(binding.tvDetUserPass.getText().toString());
                user.setCorreo(binding.tvDetUserEmail.getText().toString());
                user.setRol(binding.tvDetUserRol.getText().toString());
                user.setImgu(binding.tvDetUserUrl.getText().toString());
                user.setEstadou(binding.tvDetUserEst.getText().toString());
                Intent editarU = new Intent(getApplicationContext(), EditarUsuario.class);
                editarU.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                editarU.putExtra("editarUsuario", user);
                getApplicationContext().startActivity(editarU);
            }
        });
    }

    private void cambiarEstadoUsuario(Integer id, Usuario user) {
        Call<Usuario> call = UsuarioApiAdapter.getApiService().updUsuario(id, user);
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Cambio de estado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
            }
        });
    }

}