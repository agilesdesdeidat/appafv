package com.example.appafv.Fragments.Sistema;

import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.appafv.Utils.SistemaApiAdapter;
import com.example.appafv.Utils.SistemaApiService;
import com.example.appafv.databinding.GenerarBackupFragmentBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenerarBackup extends Fragment {

    private GenerarBackupViewModel mViewModel;
    private GenerarBackupFragmentBinding binding;

    public static GenerarBackup newInstance() {
        return new GenerarBackup();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
       binding = GenerarBackupFragmentBinding.inflate(inflater, container, false );
       return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(GenerarBackupViewModel.class);

        binding.btnGenerarBackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<String> call = SistemaApiAdapter.getService().createBackup();
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {

                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(getContext().getApplicationContext(), "Backup generado con Exito!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

}