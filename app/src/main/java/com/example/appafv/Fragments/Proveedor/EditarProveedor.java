package com.example.appafv.Fragments.Proveedor;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Proveedor;
import com.example.appafv.Utils.ProveedorApiAdapter;
import com.example.appafv.databinding.ActivityEditarProveedorBinding;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarProveedor extends AppCompatActivity {

    private ActivityEditarProveedorBinding binding;
    private int IMG_REQUEST = 21;
    private Bitmap bitmap;
    private int editImage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEditarProveedorBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("editarProveedor")) {
            Proveedor objProveedor = this.getIntent().getParcelableExtra("editarProveedor");
            binding.tvEditProvId.setText(objProveedor.getProveedorId().toString());
            binding.etEditProvEmp.setText(objProveedor.getNombreEmp());
            binding.etEditProvRuc.setText(objProveedor.getRuc());
            binding.etEditProvTel.setText(objProveedor.getTelefono());
            binding.etEditProvDir.setText(objProveedor.getDireccion());
            binding.etEditProvImg.setText(objProveedor.getImgp());
            Glide.with(this)
                    .load(objProveedor.getImgp())
                    .signature(new ObjectKey(UUID.randomUUID().toString()))
                    .into(binding.ivEditProvImg);
        }

        binding.ibEditCargaUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editImage = 1;

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMG_REQUEST);
            }
        });

        binding.btnEditarProveedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etEditProvEmp.getText().toString().isEmpty() || binding.etEditProvRuc.getText().toString().isEmpty() ||
                        binding.etEditProvRuc.getText().toString().length() != 11 || binding.etEditProvTel.getText().toString().isEmpty() ||
                        binding.etEditProvTel.getText().toString().length() != 7 || binding.etEditProvDir.getText().toString().isEmpty() ||
                        binding.ivEditProvImg.getDrawable() == null) {
                    validarDatos();
                } else {
                    Proveedor p = new Proveedor();
                    Integer id = Integer.parseInt(binding.tvEditProvId.getText().toString());
                    p.setNombreEmp(binding.etEditProvEmp.getText().toString());
                    p.setRuc(binding.etEditProvRuc.getText().toString());
                    p.setTelefono(binding.etEditProvTel.getText().toString());
                    p.setDireccion(binding.etEditProvDir.getText().toString());

                    if (editImage == 1) {
                        p.setImgp(uploadImage());
                    } else {
                        p.setImgp(binding.etEditProvImg.getText().toString());
                    }

                    updateProveedor(id, p);
                }
            }
        });

        binding.btnCancelarEditProv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditarProveedor.this, MenuPrincipalActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                binding.ivEditProvImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateProveedor(int id, Proveedor p) {
        Call<Proveedor> call = ProveedorApiAdapter.getApiService().updProveedor(id, p);
        call.enqueue(new Callback<Proveedor>() {
            @Override
            public void onResponse(Call<Proveedor> call, Response<Proveedor> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Proveedor actualizado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Proveedor> call, Throwable t) {
            }
        });
    }

    private String uploadImage() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);

        return encodedImage;
    }

    private void validarDatos() {
        if (binding.etEditProvEmp.getText().toString().isEmpty()) {
            binding.etEditProvEmp.setError("Este campo no puede estar vacio.");
        } else if (binding.etEditProvRuc.getText().toString().isEmpty()) {
            binding.etEditProvRuc.setError("Este campo no puede estar vacio.");
        } else if (binding.etEditProvRuc.getText().toString().length() != 11) {
            binding.etEditProvRuc.setError("Este campo debe contener 11 caracteres.");
        } else if (binding.etEditProvTel.getText().toString().isEmpty()) {
            binding.etEditProvTel.setError("Este campo no puede estar vacio.");
        } else if (binding.etEditProvTel.getText().toString().length() != 7) {
            binding.etEditProvTel.setError("Este campo debe contener 7 caracteres.");
        } else if (binding.etEditProvDir.getText().toString().isEmpty()) {
            binding.etEditProvDir.setError("Este campo no puede estar vacio.");
        } else if (binding.ivEditProvImg.getDrawable() == null) {
            Toast.makeText(getApplicationContext(), "Debe elegir una imagen.", Toast.LENGTH_SHORT).show();
        }
    }

}