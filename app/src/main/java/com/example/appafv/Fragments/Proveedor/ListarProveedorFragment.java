package com.example.appafv.Fragments.Proveedor;

import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;
import com.example.appafv.Adapter.ProveedorAdapter;
import com.example.appafv.Model.Proveedor;
import com.example.appafv.Utils.ProveedorApiAdapter;
import com.example.appafv.databinding.ListarProveedorFragmentBinding;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListarProveedorFragment extends Fragment {

    private ListarProveedorViewModel mViewModel;
    private ListarProveedorFragmentBinding binding;

    ArrayList<Proveedor> proveedorList;

    public static ListarProveedorFragment newInstance() {
        return new ListarProveedorFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = ListarProveedorFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ListarProveedorViewModel.class);
        // TODO: Use the ViewModel

        binding.rvProveedor.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        obtenerProveedores();

        binding.svProveedor.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<Proveedor> mlista = new ArrayList<>();
                for(Proveedor obj : proveedorList) {
                    if (obj.getNombreEmp().toLowerCase().contains(newText.toLowerCase())) {
                        mlista.add(obj);
                    }
                }
                binding.rvProveedor.setAdapter(new ProveedorAdapter(getContext().getApplicationContext(), mlista));
                return true;
            }
        });

        binding.btnReporteProveedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat formatFile = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
                String dateFile = formatFile.format(date);
                String nameFile = "Proveedor - " + dateFile + ".pdf";
                String pathFile = getContext().getExternalFilesDir("Proveedor").toString() + "/" + nameFile;
                File file = new File(pathFile);

                try {
                    Document document = new Document(PageSize.A4);
                    PdfWriter.getInstance(document, new FileOutputStream(file.getAbsoluteFile()));
                    document.open();
                    Font FontNormal = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
                    Font FontBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK);
                    Font FontBigBold = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD, BaseColor.BLACK);

                    Chunk title = new Chunk("REPORTE PROVEEDOR", FontBigBold);
                    Paragraph paragraphTitle = new Paragraph(title);
                    paragraphTitle.setAlignment(Element.ALIGN_CENTER);
                    paragraphTitle.setSpacingAfter(20);
                    document.add(paragraphTitle);

                    PdfPTable table = new PdfPTable(4);
                    table.setWidthPercentage(100);

                    String[] columns = {"ID", "NOMBRE", "RUC", "TELEFONO"};
                    PdfPCell cell;

                    for (String column : columns) {
                        cell = new PdfPCell(new Phrase(column, FontBold));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setRowspan(2);
                        table.addCell(cell);
                    }

                    for (Proveedor prov : proveedorList) {
                        cell = new PdfPCell(new Phrase(prov.getProveedorId().toString(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(prov.getNombreEmp(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(prov.getRuc(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(prov.getTelefono(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                    }

                    document.add(table);
                    document.close();
                } catch (DocumentException | FileNotFoundException e) {
                    e.printStackTrace();
                }

                Toast.makeText(getContext().getApplicationContext(), "Reporte generado correctamente", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void obtenerProveedores() {
        Call<ArrayList<Proveedor>> call = ProveedorApiAdapter.getApiService().getProveedores();
        call.enqueue(new Callback<ArrayList<Proveedor>>() {
            @Override
            public void onResponse(Call<ArrayList<Proveedor>> call, Response<ArrayList<Proveedor>> response) {
                proveedorList = response.body();
                binding.rvProveedor.setAdapter(new ProveedorAdapter(getContext().getApplicationContext(), proveedorList));
            }

            @Override
            public void onFailure(Call<ArrayList<Proveedor>> call, Throwable t) {
            }
        });
    }

}