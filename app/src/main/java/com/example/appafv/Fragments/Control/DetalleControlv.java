package com.example.appafv.Fragments.Control;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.appafv.Fragments.Mantenimiento.DetalleMant;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.ControlVehiculo;
import com.example.appafv.Model.MantVehiculo;
import com.example.appafv.R;
import com.example.appafv.Utils.ControlApiAdapter;
import com.example.appafv.Utils.MantApiAdapter;
import com.example.appafv.databinding.ActivityDetalleControlvBinding;

import java.util.Calendar;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetalleControlv extends AppCompatActivity {

    private ActivityDetalleControlvBinding binding;

    String horaE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetalleControlvBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("detallControl")) {
            ControlVehiculo objControl = this.getIntent().getParcelableExtra("detallControl");
            binding.tvDetControlvId.setText(objControl.getControlId().toString());
            binding.tvDetControlvVeh.setText(objControl.getVehiculo_vehiculoId());
            binding.tvDetControlvEmp.setText(objControl.getEmpleado_empleadoId());
            binding.tvDetControlvFecha.setText(objControl.getFecha());
            binding.tvDetControlvSalida.setText(objControl.getHorasalida());
            binding.tvDetControlvEntrada.setText(objControl.getHoraentrada());
            binding.tvDetControlvEst.setText(objControl.getEstado_Control_estadocId().toString());
            Glide.with(this)
                    .load(objControl.getImgv())
                    .into(binding.ivDetControlvImg);
        }

        if (!binding.tvDetControlvEntrada.getText().toString().equals("")) {
            binding.btnDetControlEst.setVisibility(View.INVISIBLE);
        }

        binding.btnDetControlDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(DetalleControlv.this);
                dialog.setTitle("ALERTA DE ELIMINACION");
                dialog.setMessage("Estas a punto de Eliminar este Control. ¿Quieres continuar?");
                dialog.setCancelable(false);
                dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(DetalleControlv.this, "Operacion cancelada", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Call<ControlVehiculo> call = ControlApiAdapter.getApiService().delControlv(Integer.parseInt(binding.tvDetControlvId.getText().toString()));
                        call.enqueue(new Callback<ControlVehiculo>() {
                            @Override
                            public void onResponse(Call<ControlVehiculo> call, Response<ControlVehiculo> response) {
                                if (response.isSuccessful()) {
                                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(), "Control Eliminado con Exito!", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ControlVehiculo> call, Throwable t) {

                            }
                        });
                    }
                });
                AlertDialog dialog1 = dialog.create();
                dialog1.show();
            }
        });

        binding.btnDetControlEst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(DetalleControlv.this);
                dialog.setTitle("ALERTA DE CAMBIO DE ESTADO");
                dialog.setMessage("Esta a punto de Temrinar el Control, es decir, que se el Vehículo Regreso a la Sede. ¿Quieres continuar?");
                dialog.setCancelable(false);
                dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(DetalleControlv.this, "Operacion cancelada", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        final TimeZone tz = TimeZone.getTimeZone("GMT-5");
                        final Calendar c = Calendar.getInstance(tz);
                        int mHour = c.get(Calendar.HOUR_OF_DAY);
                        int mMinute = c.get(Calendar.MINUTE);

                        horaE = String.format("%02d:%02d", mHour , mMinute);

                        ControlVehiculo cv = new ControlVehiculo();
                        Integer id = Integer.parseInt(binding.tvDetControlvId.getText().toString());
                        cv.setVehiculo_vehiculoId(binding.tvDetControlvVeh.getText().toString());
                        cv.setEmpleado_empleadoId(binding.tvDetControlvEmp.getText().toString());
                        cv.setFecha(binding.tvDetControlvFecha.getText().toString());
                        cv.setHorasalida(binding.tvDetControlvSalida.getText().toString());
                        cv.setHoraentrada(horaE);
                        if (binding.tvDetControlvEst.getText().toString().equals("1")) {
                            cv.setEstado_Control_estadocId(2);
                        } else {
                            cv.setEstado_Control_estadocId(1);
                        }

                        Call<ControlVehiculo> call = ControlApiAdapter.getApiService().updControlv(id, cv);
                        call.enqueue(new Callback<ControlVehiculo>() {
                            @Override
                            public void onResponse(Call<ControlVehiculo> call, Response<ControlVehiculo> response) {
                                if(response.isSuccessful()) {
                                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(), "Control Concluido con exito!", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ControlVehiculo> call, Throwable t) {

                            }
                        });
                    }
                });
                AlertDialog dialog1 = dialog.create();
                dialog1.show();
            }
        });

    }
}