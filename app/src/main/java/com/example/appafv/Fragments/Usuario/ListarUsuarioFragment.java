package com.example.appafv.Fragments.Usuario;

import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import com.example.appafv.Adapter.UsuarioAdapter;
import com.example.appafv.Model.Usuario;
import com.example.appafv.Utils.UsuarioApiAdapter;
import com.example.appafv.databinding.ListarUsuarioFragmentBinding;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListarUsuarioFragment extends Fragment {

    private ListarUsuarioViewModel mViewModel;
    private ListarUsuarioFragmentBinding binding;

    ArrayList<Usuario> usuarioList;

    public static ListarUsuarioFragment newInstance() {
        return new ListarUsuarioFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = ListarUsuarioFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ListarUsuarioViewModel.class);
        // TODO: Use the ViewModel

        binding.rvUsuario.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        obtenerUsuario();

        binding.svUsuario.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<Usuario> userlist = new ArrayList<>();
                for(Usuario obj : usuarioList) {
                    if (obj.getNombrec().toLowerCase().contains(newText.toLowerCase())) {
                        userlist.add(obj);
                    }
                }
                binding.rvUsuario.setAdapter(new UsuarioAdapter(getContext().getApplicationContext(), userlist));
                return true;
            }
        });
    }

    private void obtenerUsuario() {
        Call<ArrayList<Usuario>> call = UsuarioApiAdapter.getApiService().getUsuarios();
        call.enqueue(new Callback<ArrayList<Usuario>>() {
            @Override
            public void onResponse(Call<ArrayList<Usuario>> call, Response<ArrayList<Usuario>> response) {
                usuarioList = response.body();
                binding.rvUsuario.setAdapter(new UsuarioAdapter(getContext().getApplicationContext(), usuarioList));
            }

            @Override
            public void onFailure(Call<ArrayList<Usuario>> call, Throwable t) {
            }
        });
    }

}