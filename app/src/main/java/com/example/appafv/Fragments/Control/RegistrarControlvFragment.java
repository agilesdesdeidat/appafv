package com.example.appafv.Fragments.Control;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.ControlVehiculo;
import com.example.appafv.Model.Empleado;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.R;
import com.example.appafv.Utils.ControlApiAdapter;
import com.example.appafv.Utils.EmpleadoApiAdapter;
import com.example.appafv.Utils.VehiculoApiAdapter;
import com.example.appafv.databinding.RegistrarControlvFragmentBinding;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrarControlvFragment extends Fragment {

    private RegistrarControlvViewModel mViewModel;
    private RegistrarControlvFragmentBinding binding;

    String horaS;

    public static RegistrarControlvFragment newInstance() {
        return new RegistrarControlvFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = RegistrarControlvFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(RegistrarControlvViewModel.class);
        // TODO: Use the ViewModel

        obtenerVehiculoDisponible();
        obtenerEmpleadoConductor();

        final TimeZone tz = TimeZone.getTimeZone("GMT-5");
        final Calendar c = Calendar.getInstance(tz);
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        mes = mes + 1;

        binding.tvAddControlFecha.setText(String.format("%02d-%02d-%02d", anio, mes, dia));
        horaS = String.format("%02d:%02d", mHour , mMinute);

        binding.btnAddControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlVehiculo cv = new ControlVehiculo();
                cv.setVehiculo_vehiculoId(binding.spAddControlVeh.getSelectedItem().toString());
                cv.setEmpleado_empleadoId(binding.spAddControlEmp.getSelectedItem().toString());
                cv.setFecha(binding.tvAddControlFecha.getText().toString());
                cv.setHorasalida(horaS);
                cv.setHoraentrada("");
                cv.setEstado_Control_estadocId(1);
                addControlv(cv);
            }
        });

    }

    private void addControlv(ControlVehiculo cv) {
        Call<ControlVehiculo> call = ControlApiAdapter.getApiService().addControlv(cv);
        call.enqueue(new Callback<ControlVehiculo>() {
            @Override
            public void onResponse(Call<ControlVehiculo> call, Response<ControlVehiculo> response) {
                Intent intent = new Intent(getContext().getApplicationContext(), MenuPrincipalActivity.class);
                startActivity(intent);
                Toast.makeText(getContext().getApplicationContext(), "Control Iniciado Con Exito!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ControlVehiculo> call, Throwable t) {

            }
        });
    }

    private void obtenerEmpleadoConductor() {
        Call<ArrayList<Empleado>> call = EmpleadoApiAdapter.getApiService().getEmpleadoConductor();
        call.enqueue(new Callback<ArrayList<Empleado>>() {
            @Override
            public void onResponse(Call<ArrayList<Empleado>> call, Response<ArrayList<Empleado>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Empleado> empCon = response.body();
                    List<String> list = new ArrayList<>();
                    for (Empleado e : empCon) {
                        list.add(e.getNombrec());
                    }
                    ArrayAdapter<String> spArrayAdapter= new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddControlEmp.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Empleado>> call, Throwable t) {

            }
        });
    }

    private void obtenerVehiculoDisponible() {
        Call<ArrayList<Vehiculo>> call = VehiculoApiAdapter.getApiService().getVehiculoDisponible();
        call.enqueue(new Callback<ArrayList<Vehiculo>>() {
            @Override
            public void onResponse(Call<ArrayList<Vehiculo>> call, Response<ArrayList<Vehiculo>> response) {
                if(response.isSuccessful()) {
                    ArrayList<Vehiculo> vehiculoPlaca = response.body();
                    List<String> list = new ArrayList<>();
                    for (Vehiculo v : vehiculoPlaca) {
                        list.add(v.getPlaca());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddControlVeh.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Vehiculo>> call, Throwable t) {

            }
        });
    }

}