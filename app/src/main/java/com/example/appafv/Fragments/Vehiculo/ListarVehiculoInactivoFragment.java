package com.example.appafv.Fragments.Vehiculo;

import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;
import com.example.appafv.Adapter.VehiculoAdapter;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.Utils.VehiculoApiAdapter;
import com.example.appafv.databinding.ListarVehiculoInactivoFragmentBinding;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListarVehiculoInactivoFragment extends Fragment {

    private ListarVehiculoInactivoViewModel mViewModel;
    private ListarVehiculoInactivoFragmentBinding binding;

    ArrayList<Vehiculo> vehiculoList;

    public static ListarVehiculoInactivoFragment newInstance() {
        return new ListarVehiculoInactivoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = ListarVehiculoInactivoFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ListarVehiculoInactivoViewModel.class);
        // TODO: Use the ViewModel

        binding.rvVehiculoI.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        obtenerVehiculoInactivo();

        binding.svVehiculoI.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<Vehiculo> vdlista = new ArrayList<>();
                for (Vehiculo obj : vehiculoList) {
                    if (obj.getPlaca().toLowerCase().contains(newText.toLowerCase())) {
                        vdlista.add(obj);
                    }
                }
                binding.rvVehiculoI.setAdapter(new VehiculoAdapter(getContext().getApplicationContext(), vdlista));
                return true;
            }
        });

        binding.btnReporteVehiculoI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat formatFile = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
                String dateFile = formatFile.format(date);
                String nameFile = "VehiculoInactivo - " + dateFile + ".pdf";
                String pathFile = getContext().getExternalFilesDir("Vehiculo").toString() + "/" + nameFile;
                File file = new File(pathFile);

                try {
                    Document document = new Document(PageSize.A4);
                    PdfWriter.getInstance(document, new FileOutputStream(file.getAbsoluteFile()));
                    document.open();
                    Font FontNormal = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
                    Font FontBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK);
                    Font FontBigBold = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD, BaseColor.BLACK);

                    Chunk title = new Chunk("REPORTE VEHICULO INACTIVO", FontBigBold);
                    Paragraph paragraphTitle = new Paragraph(title);
                    paragraphTitle.setAlignment(Element.ALIGN_CENTER);
                    paragraphTitle.setSpacingAfter(20);
                    document.add(paragraphTitle);

                    PdfPTable table = new PdfPTable(4);
                    table.setWidthPercentage(100);

                    String[] columns = {"PLACA", "PROVEEDOR", "FABRICACION", "MARCA"};
                    PdfPCell cell;

                    for (String column : columns) {
                        cell = new PdfPCell(new Phrase(column, FontBold));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setRowspan(2);
                        table.addCell(cell);
                    }

                    for (Vehiculo veh : vehiculoList) {
                        cell = new PdfPCell(new Phrase(veh.getPlaca(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(veh.getProveedor_proveedorId(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(veh.getAnioFabri().toString(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(veh.getMarca(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                    }

                    document.add(table);
                    document.close();
                } catch (DocumentException | FileNotFoundException e) {
                    e.printStackTrace();
                }

                Toast.makeText(getContext().getApplicationContext(), "Reporte generado correctamente", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void obtenerVehiculoInactivo() {
        Call<ArrayList<Vehiculo>> call = VehiculoApiAdapter.getApiService().getVehiculoNoDisponible();
        call.enqueue(new Callback<ArrayList<Vehiculo>>() {
            @Override
            public void onResponse(Call<ArrayList<Vehiculo>> call, Response<ArrayList<Vehiculo>> response) {
                vehiculoList = response.body();
                binding.rvVehiculoI.setAdapter(new VehiculoAdapter(getContext().getApplicationContext(), vehiculoList));
            }

            @Override
            public void onFailure(Call<ArrayList<Vehiculo>> call, Throwable t) {

            }
        });
    }

}