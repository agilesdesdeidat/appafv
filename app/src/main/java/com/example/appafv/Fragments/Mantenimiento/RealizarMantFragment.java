package com.example.appafv.Fragments.Mantenimiento;

import androidx.lifecycle.ViewModelProvider;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Empleado;
import com.example.appafv.Model.MantVehiculo;
import com.example.appafv.Model.MotivoMant;
import com.example.appafv.Model.TipoMant;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.Utils.EmpleadoApiAdapter;
import com.example.appafv.Utils.MantApiAdapter;
import com.example.appafv.Utils.MotivoMantApiAdapter;
import com.example.appafv.Utils.TipoMantApiAdapter;
import com.example.appafv.Utils.VehiculoApiAdapter;
import com.example.appafv.databinding.RealizarMantFragmentBinding;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RealizarMantFragment extends Fragment {

    private RealizarMantViewModel mViewModel;
    private RealizarMantFragmentBinding binding;

    public static RealizarMantFragment newInstance() {
        return new RealizarMantFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = RealizarMantFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(RealizarMantViewModel.class);
        // TODO: Use the ViewModel

        obtenerVehiculoDisponible();
        obtenerEmpleadoMecanico();
        obtenerTipoMantV();
        obtenerMotivoMantV();

        binding.btnMantFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dia, mes, ano;
                final Calendar c = Calendar.getInstance();
                dia = c.get(Calendar.DAY_OF_MONTH);
                mes = c.get(Calendar.MONTH);
                ano = c.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        binding.etAddMantFecha.setText(year + "-" + (month+1) + "-" + dayOfMonth);
                    }
                }, dia, mes, ano);
                datePickerDialog.show();
            }
        });

        binding.ibCargaMantUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(binding.etAddMantUrl.getText().toString().isEmpty()) {
                    Toast.makeText(getContext().getApplicationContext(), "No ha colocado la URL.", Toast.LENGTH_SHORT).show();
                } else {
                    Glide.with(getContext().getApplicationContext())
                            .load(binding.etAddMantUrl.getText().toString())
                            .into(binding.ivAddMant);
                }
            }
        });

        binding.btnRegistrarMant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etAddMantFecha.getText().toString().isEmpty() || binding.etAddMantPre.getText().toString().isEmpty() ||
                        binding.etAddMantUrl.getText().toString().isEmpty() || binding.etAddMantObs.getText().toString().isEmpty() ||
                        binding.ivAddMant.getDrawable() == null) {
                    validarDatos();
                } else {
                    MantVehiculo mv = new MantVehiculo();
                    mv.setVehiculo_vehiculoId(binding.spAddMantVeh.getSelectedItem().toString());
                    mv.setEmpleado_empleadoId(binding.spAddMantEmp.getSelectedItem().toString());
                    mv.setFecha(binding.etAddMantFecha.getText().toString());
                    mv.setTipo_Mant_tipomId(binding.spAddMantTipo.getSelectedItem().toString());
                    mv.setMotivo_Mant_motivomId(binding.spAddMantMotivo.getSelectedItem().toString());
                    mv.setPrecio(Double.parseDouble(binding.etAddMantPre.getText().toString()));
                    mv.setImgm(binding.etAddMantUrl.getText().toString());
                    mv.setObservacion(binding.etAddMantObs.getText().toString());
                    mv.setEstado_Mant_estadomId(1);
                    addMantVehiculo(mv);
                }
            }
        });
    }

    private void addMantVehiculo(MantVehiculo mv) {
        Call<MantVehiculo> call = MantApiAdapter.getApiService().addMantVehiculo(mv);
        call.enqueue(new Callback<MantVehiculo>() {
            @Override
            public void onResponse(Call<MantVehiculo> call, Response<MantVehiculo> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getContext().getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getContext().getApplicationContext(), "Mant. Vehiculo registrado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MantVehiculo> call, Throwable t) {
            }
        });
    }

    private void obtenerMotivoMantV() {
        Call<ArrayList<MotivoMant>> call = MotivoMantApiAdapter.getApiService().getMotivoMant();
        call.enqueue(new Callback<ArrayList<MotivoMant>>() {
            @Override
            public void onResponse(Call<ArrayList<MotivoMant>> call, Response<ArrayList<MotivoMant>> response) {
                if (response.isSuccessful()) {
                    ArrayList<MotivoMant> motivoDesc = response.body();
                    List<String> list = new ArrayList<>();
                    for (MotivoMant mm : motivoDesc) {
                        list.add(mm.getDescripcion());
                    }
                    ArrayAdapter<String> spArrayAdapter= new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddMantMotivo.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MotivoMant>> call, Throwable t) {
            }
        });
    }

    private void obtenerTipoMantV() {
        Call<ArrayList<TipoMant>> call = TipoMantApiAdapter.getApiService().getTipoMant();
        call.enqueue(new Callback<ArrayList<TipoMant>>() {
            @Override
            public void onResponse(Call<ArrayList<TipoMant>> call, Response<ArrayList<TipoMant>> response) {
                if (response.isSuccessful()) {
                    ArrayList<TipoMant> tipoDesc = response.body();
                    List<String> list = new ArrayList<>();
                    for (TipoMant tm : tipoDesc) {
                        list.add(tm.getDescripcion());
                    }
                    ArrayAdapter<String> spArrayAdapter= new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddMantTipo.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TipoMant>> call, Throwable t) {
            }
        });
    }

    private void obtenerEmpleadoMecanico() {
        Call<ArrayList<Empleado>> call = EmpleadoApiAdapter.getApiService().getEmpleadoMecanico();
        call.enqueue(new Callback<ArrayList<Empleado>>() {
            @Override
            public void onResponse(Call<ArrayList<Empleado>> call, Response<ArrayList<Empleado>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Empleado> empleadoMec = response.body();
                    List<String> list = new ArrayList<>();
                    for (Empleado e : empleadoMec) {
                        list.add(e.getNombrec());
                    }
                    ArrayAdapter<String> spArrayAdapter= new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddMantEmp.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Empleado>> call, Throwable t) {
            }
        });
    }

    private void obtenerVehiculoDisponible() {
        Call<ArrayList<Vehiculo>> call = VehiculoApiAdapter.getApiService().getVehiculos();
        call.enqueue(new Callback<ArrayList<Vehiculo>>() {
            @Override
            public void onResponse(Call<ArrayList<Vehiculo>> call, Response<ArrayList<Vehiculo>> response) {
                if(response.isSuccessful()) {
                    ArrayList<Vehiculo> vehiculoPlaca = response.body();
                    List<String> list = new ArrayList<>();
                    for (Vehiculo v : vehiculoPlaca) {
                        list.add(v.getPlaca());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddMantVeh.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Vehiculo>> call, Throwable t) {
            }
        });
    }

    private void validarDatos() {
        if (binding.etAddMantFecha.getText().toString().isEmpty()) {
            binding.etAddMantFecha.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddMantPre.getText().toString().isEmpty()) {
            binding.etAddMantPre.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddMantUrl.getText().toString().isEmpty()) {
            binding.etAddMantUrl.setError("Este campo no puede estar vacio.");
        } else if (binding.etAddMantObs.getText().toString().isEmpty()) {
            binding.etAddMantObs.setError("Este campo no puede estar vacio.");
        } else if (binding.ivAddMant.getDrawable() == null) {
            Toast.makeText(getContext().getApplicationContext(), "Debe visualizar la URL.", Toast.LENGTH_SHORT).show();
        }
    }

}