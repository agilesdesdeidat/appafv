package com.example.appafv.Fragments.Vehiculo;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.R;
import com.example.appafv.Utils.VehiculoApiService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReporteVehiculo extends AppCompatActivity {
    Button export;


    TextView textView, tvtitulo, tvcolumna, horafecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte_vehiculo);
        textView = findViewById(R.id.tvresult);
        export = findViewById(R.id.btncrear);
        tvcolumna = findViewById(R.id.tvcolumna);
        tvtitulo = findViewById(R.id.titulorep);
        horafecha = findViewById(R.id.tvhora);

        Date date = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss a");
        String dateNow = formatDate.format(date);

        horafecha.setText(dateNow);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://taitacompany.000webhostapp.com/api_final/vehiculo/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        VehiculoApiService jsonPlaceHolderApi = retrofit.create(VehiculoApiService.class);

        Call<ArrayList<Vehiculo>> call = jsonPlaceHolderApi.getVehiculoDisponible();

        call.enqueue(new Callback<ArrayList<Vehiculo>>() {
            @Override
            public void onResponse(Call<ArrayList<Vehiculo>> call, Response<ArrayList<Vehiculo>> response) {
                if (!response.isSuccessful()) {
                    textView.setText("Code: " + response.code());
                    return;
                }

                ArrayList<Vehiculo> vehiculos = response.body();

                for (Vehiculo vehiculo : vehiculos) {
                    String list = "";
                    list += "   " + vehiculo.getPlaca() + "     " + vehiculo.getProveedor_proveedorId() + "       " + vehiculo.getAnioFabri() + "         " + vehiculo.getMarca() + "\n";
                    textView.append(list);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Vehiculo>> call, Throwable t) {
                textView.setText(t.getMessage());
            }
        });

        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat formatFile = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
                String dateFile = formatFile.format(date);
                String nameFile = "Vehiculos - " + dateFile + ".pdf";
                String path = getExternalFilesDir("ReporteVehiculoDisponible").toString() + "/" + nameFile;

                File file = new File(path);

                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                Document document = new Document(PageSize.A4);

                try {
                    PdfWriter.getInstance(document, new FileOutputStream(file.getAbsoluteFile()));
                } catch (DocumentException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                document.open();

                try {
                    document.add(new Paragraph(tvtitulo.getText().toString()));
                    document.add(new Paragraph("\n"));
                    document.add(new Paragraph("     " + tvcolumna.getText().toString()));
                    document.add(new Paragraph("\n"));
                    document.add(new Paragraph(textView.getText().toString()));
                    document.add(new Paragraph("\n"));
                    document.add(new Paragraph("Fecha de Reporte: " + horafecha.getText().toString()));
                } catch (DocumentException e) {
                    e.printStackTrace();
                }

                Toast.makeText(getApplicationContext(), "Archivo exportado correctamente", Toast.LENGTH_LONG).show();
                document.close();
            }
        });
    }

}