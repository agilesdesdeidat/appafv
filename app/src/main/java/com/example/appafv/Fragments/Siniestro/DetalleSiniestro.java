package com.example.appafv.Fragments.Siniestro;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.Model.Siniestro;
import com.example.appafv.databinding.ActivityDetalleSiniestroBinding;
import java.util.UUID;

public class DetalleSiniestro extends AppCompatActivity {

    private ActivityDetalleSiniestroBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetalleSiniestroBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("detalleSin")) {
            Siniestro objSin = this.getIntent().getParcelableExtra("detalleSin");
            binding.tvDetSinId.setText(objSin.getSiniestroId().toString());
            binding.tvDetSinVeh.setText(objSin.getVehiculo_vehiculoId());
            binding.tvDetSinEmp.setText(objSin.getEmpleado_empleadoId());
            binding.tvDetSinFecha.setText(objSin.getFecha());
            binding.tvDetSinTipo.setText(objSin.getTipo());
            binding.tvDetSinMotivo.setText(objSin.getMotivo_Siniestro_motivosId());
            binding.tvDetSinUrl.setText(objSin.getImgs());
            binding.tvDetSinDetalle.setText(objSin.getDetalle());
            Glide.with(this)
                    .load(objSin.getImgs())
                    .signature(new ObjectKey(UUID.randomUUID().toString()))
                    .into(binding.ivDetSinImg);
        }

        binding.btnDetSinEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Siniestro sv = new Siniestro();
                sv.setSiniestroId(Integer.parseInt(binding.tvDetSinId.getText().toString()));
                sv.setVehiculo_vehiculoId(binding.tvDetSinVeh.getText().toString());
                sv.setEmpleado_empleadoId(binding.tvDetSinEmp.getText().toString());
                sv.setFecha(binding.tvDetSinFecha.getText().toString());
                sv.setTipo(binding.tvDetSinTipo.getText().toString());
                sv.setMotivo_Siniestro_motivosId(binding.tvDetSinMotivo.getText().toString());
                sv.setImgs(binding.tvDetSinUrl.getText().toString());
                sv.setDetalle(binding.tvDetSinDetalle.getText().toString());

                Intent editSin = new Intent(getApplicationContext(), EditarSiniestro.class);
                editSin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                editSin.putExtra("editSin", sv);
                getApplicationContext().startActivity(editSin);
            }
        });
    }

}