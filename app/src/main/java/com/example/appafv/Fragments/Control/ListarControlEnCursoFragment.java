package com.example.appafv.Fragments.Control;

import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import com.example.appafv.Adapter.ControlAdapter;
import com.example.appafv.Model.Control;
import com.example.appafv.Utils.ControlApiAdapter;
import com.example.appafv.databinding.ListarControlEnCursoFragmentBinding;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListarControlEnCursoFragment extends Fragment {

    private ListarControlEnCursoViewModel mViewModel;
    private ListarControlEnCursoFragmentBinding binding;

    ArrayList<Control> controlList;

    public static ListarControlEnCursoFragment newInstance() {
        return new ListarControlEnCursoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = ListarControlEnCursoFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ListarControlEnCursoViewModel.class);
        // TODO: Use the ViewModel

        binding.rvControlEC.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        obtenerControlEnCurso();
        binding.svControlEC.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<Control> clist = new ArrayList<>();
                for (Control obj : controlList) {
                    if (obj.getVehiculo_vehiculoId().toLowerCase().contains(newText.toLowerCase())) {
                        clist.add(obj);
                    }
                }
                binding.rvControlEC.setAdapter(new ControlAdapter(getContext().getApplicationContext(), clist));
                return true;
            }
        });
    }

    private void obtenerControlEnCurso() {
        Call<ArrayList<Control>> call = ControlApiAdapter.getApiService().getControlEnCurso();
        call.enqueue(new Callback<ArrayList<Control>>() {
            @Override
            public void onResponse(Call<ArrayList<Control>> call, Response<ArrayList<Control>> response) {
                controlList = response.body();
                binding.rvControlEC.setAdapter(new ControlAdapter(getContext().getApplicationContext(), controlList));
            }

            @Override
            public void onFailure(Call<ArrayList<Control>> call, Throwable t) {
            }
        });
    }

}