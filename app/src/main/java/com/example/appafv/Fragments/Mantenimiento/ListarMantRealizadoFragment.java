package com.example.appafv.Fragments.Mantenimiento;

import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;
import com.example.appafv.Adapter.MantenimientoAdapter;
import com.example.appafv.Model.Mantenimiento;
import com.example.appafv.Utils.MantApiAdapter;
import com.example.appafv.databinding.ListarMantRealizadoFragmentBinding;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListarMantRealizadoFragment extends Fragment {

    private ListarMantRealizadoViewModel mViewModel;
    private ListarMantRealizadoFragmentBinding binding;

    ArrayList<Mantenimiento> mantList;

    public static ListarMantRealizadoFragment newInstance() {
        return new ListarMantRealizadoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = ListarMantRealizadoFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ListarMantRealizadoViewModel.class);
        // TODO: Use the ViewModel

        binding.rvMantenimientoR.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        obtenerMantRealizado();

        binding.svMantenimientoR.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<Mantenimiento> mvlist = new ArrayList<>();
                for (Mantenimiento obj : mantList) {
                    if (obj.getVehiculo_vehiculoId().toLowerCase().contains(newText.toLowerCase())) {
                        mvlist.add(obj);
                    }
                }
                binding.rvMantenimientoR.setAdapter(new MantenimientoAdapter(getContext().getApplicationContext(), mvlist));
                return true;
            }
        });

        binding.btnReporteMantR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = new Date();
                SimpleDateFormat formatFile = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
                String dateFile = formatFile.format(date);
                String nameFile = "MantenimientoRealizado - " + dateFile + ".pdf";
                String pathFile = getContext().getExternalFilesDir("Mantenimiento").toString() + "/" + nameFile;
                File file = new File(pathFile);

                try {
                    Document document = new Document(PageSize.A4);
                    PdfWriter.getInstance(document, new FileOutputStream(file.getAbsoluteFile()));
                    document.open();
                    Font FontNormal = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
                    Font FontBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK);
                    Font FontBigBold = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD, BaseColor.BLACK);

                    Chunk title = new Chunk("REPORTE MANTENIMIENTO REALIZADO", FontBigBold);
                    Paragraph paragraphTitle = new Paragraph(title);
                    paragraphTitle.setAlignment(Element.ALIGN_CENTER);
                    paragraphTitle.setSpacingAfter(20);
                    document.add(paragraphTitle);

                    PdfPTable table = new PdfPTable(4);
                    table.setWidthPercentage(100);

                    String[] columns = {"PLACA", "MECANICO", "MOTIVO", "FECHA"};
                    PdfPCell cell;

                    for (String column : columns) {
                        cell = new PdfPCell(new Phrase(column, FontBold));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setRowspan(2);
                        table.addCell(cell);
                    }

                    for (Mantenimiento mant : mantList) {
                        cell = new PdfPCell(new Phrase(mant.getVehiculo_vehiculoId(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(mant.getEmpleado_empleadoId(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(mant.getMotivo_Mant_motivomId(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(mant.getFecha(), FontNormal));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                    }

                    document.add(table);
                    document.close();
                } catch (DocumentException | FileNotFoundException e) {
                    e.printStackTrace();
                }

                Toast.makeText(getContext().getApplicationContext(), "Reporte generado correctamente", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void obtenerMantRealizado() {
        Call<ArrayList<Mantenimiento>> call = MantApiAdapter.getApiService().getMantenimientoRealizado();
        call.enqueue(new Callback<ArrayList<Mantenimiento>>() {
            @Override
            public void onResponse(Call<ArrayList<Mantenimiento>> call, Response<ArrayList<Mantenimiento>> response) {
                mantList = response.body();
                binding.rvMantenimientoR.setAdapter(new MantenimientoAdapter(getContext().getApplicationContext(), mantList));
            }

            @Override
            public void onFailure(Call<ArrayList<Mantenimiento>> call, Throwable t) {
            }
        });
    }

}