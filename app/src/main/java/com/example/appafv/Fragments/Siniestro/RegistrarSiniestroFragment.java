package com.example.appafv.Fragments.Siniestro;

import androidx.lifecycle.ViewModelProvider;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Empleado;
import com.example.appafv.Model.MotivoSin;
import com.example.appafv.Model.Siniestro;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.Utils.EmpleadoApiAdapter;
import com.example.appafv.Utils.MotivoSinApiAdapter;
import com.example.appafv.Utils.SiniestroApiAdapter;
import com.example.appafv.Utils.VehiculoApiAdapter;
import com.example.appafv.databinding.RegistrarSiniestroFragmentBinding;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrarSiniestroFragment extends Fragment {

    private RegistrarSiniestroViewModel mViewModel;
    private RegistrarSiniestroFragmentBinding binding;
    private int IMG_REQUEST = 21;
    private Bitmap bitmap;

    public static RegistrarSiniestroFragment newInstance() {
        return new RegistrarSiniestroFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = RegistrarSiniestroFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(RegistrarSiniestroViewModel.class);
        // TODO: Use the ViewModel

        obtenerVehiculoDisponible();
        obtenerConductorVehiculo();
        obtenerTipoSin();
        obtenerMotivoSin();

        binding.btnAddSinFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dia, mes, ano;
                final Calendar c = Calendar.getInstance();
                dia = c.get(Calendar.DAY_OF_MONTH);
                mes = c.get(Calendar.MONTH);
                ano = c.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        binding.etAddSinFecha.setText(year + "-" + (month+1) + "-" + dayOfMonth);
                    }
                }, dia, mes, ano);
                datePickerDialog.show();
            }
        });

        binding.ibAddCargaUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMG_REQUEST);
            }
        });

        binding.btnAddSin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etAddSinFecha.getText().toString().isEmpty() || binding.ivAddSinImg.getDrawable() == null ||
                        binding.etAddSinDetalle.getText().toString().isEmpty()) {
                    validarDatos();
                } else {
                    Siniestro sv = new Siniestro();
                    sv.setVehiculo_vehiculoId(binding.spAddSinVeh.getSelectedItem().toString());
                    sv.setEmpleado_empleadoId(binding.spAddSinEmp.getSelectedItem().toString());
                    sv.setFecha(binding.etAddSinFecha.getText().toString());
                    sv.setTipo(binding.spAddSinTipo.getSelectedItem().toString());
                    sv.setMotivo_Siniestro_motivosId(binding.spAddSinMotivo.getSelectedItem().toString());
                    sv.setImgs(uploadImage());
                    sv.setDetalle(binding.etAddSinDetalle.getText().toString());
                    addSinVehiculo(sv);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), path);
                binding.ivAddSinImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addSinVehiculo(Siniestro sv) {
        Call<Siniestro> call = SiniestroApiAdapter.getApiService().addSiniestro(sv);
        call.enqueue(new Callback<Siniestro>() {
            @Override
            public void onResponse(Call<Siniestro> call, Response<Siniestro> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getContext().getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getContext().getApplicationContext(), "Siniestro Vehiculo registado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Siniestro> call, Throwable t) {
            }
        });
    }

    private void obtenerMotivoSin() {
        Call<ArrayList<MotivoSin>> call = MotivoSinApiAdapter.getApiService().getMotivoSin();
        call.enqueue(new Callback<ArrayList<MotivoSin>>() {
            @Override
            public void onResponse(Call<ArrayList<MotivoSin>> call, Response<ArrayList<MotivoSin>> response) {
                ArrayList<MotivoSin> listMot = response.body();
                List<String> list = new ArrayList<>();
                for (MotivoSin ms : listMot) {
                    list.add(ms.getDescripcion());
                }
                ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                binding.spAddSinMotivo.setAdapter(spArrayAdapter);
            }

            @Override
            public void onFailure(Call<ArrayList<MotivoSin>> call, Throwable t) {
            }
        });
    }

    private void obtenerTipoSin() {
        List<String> listTipo = new ArrayList<>();
        listTipo.add("Leve");
        listTipo.add("Medio");
        listTipo.add("Grave");
        ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, listTipo);
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spAddSinTipo.setAdapter(spArrayAdapter);
    }

    private void obtenerConductorVehiculo() {
        Call<ArrayList<Empleado>> call = EmpleadoApiAdapter.getApiService().getEmpleadoConductor();
        call.enqueue(new Callback<ArrayList<Empleado>>() {
            @Override
            public void onResponse(Call<ArrayList<Empleado>> call, Response<ArrayList<Empleado>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Empleado> empCon = response.body();
                    List<String> list = new ArrayList<>();
                    for (Empleado e : empCon) {
                        list.add(e.getNombrec());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddSinEmp.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Empleado>> call, Throwable t) {
            }
        });
    }

    private void obtenerVehiculoDisponible() {
        Call<ArrayList<Vehiculo>> call = VehiculoApiAdapter.getApiService().getVehiculoDisponible();
        call.enqueue(new Callback<ArrayList<Vehiculo>>() {
            @Override
            public void onResponse(Call<ArrayList<Vehiculo>> call, Response<ArrayList<Vehiculo>> response) {
                if(response.isSuccessful()) {
                    ArrayList<Vehiculo> vehiculoPlaca = response.body();
                    List<String> list = new ArrayList<>();
                    for (Vehiculo v : vehiculoPlaca) {
                        list.add(v.getPlaca());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getContext().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spAddSinVeh.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Vehiculo>> call, Throwable t) {
            }
        });
    }

    private String uploadImage() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);

        return encodedImage;
    }

    private void validarDatos() {
        if (binding.etAddSinFecha.getText().toString().isEmpty()) {
            binding.etAddSinFecha.setError("Este campo no puede estar vacio.");
        } else if (binding.ivAddSinImg.getDrawable() == null) {
            Toast.makeText(getContext().getApplicationContext(), "Debe elegir una imagen.", Toast.LENGTH_SHORT).show();
        } else if (binding.etAddSinDetalle.getText().toString().isEmpty()) {
            binding.etAddSinDetalle.setError("Este campo no puede estar vacio.");
        }
    }

}