package com.example.appafv.Fragments.Control;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Control;
import com.example.appafv.Utils.ControlApiAdapter;
import com.example.appafv.databinding.ActivityDetalleControlBinding;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetalleControl extends AppCompatActivity {

    private ActivityDetalleControlBinding binding;

    String horaE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetalleControlBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("detalleControl")) {
            Control objControl = this.getIntent().getParcelableExtra("detalleControl");
            binding.tvDetControlId.setText(objControl.getControlId().toString());
            binding.tvDetControlVeh.setText(objControl.getVehiculo_vehiculoId());
            binding.tvDetControlEmp.setText(objControl.getEmpleado_empleadoId());
            binding.tvDetControlFecha.setText(objControl.getFecha());
            binding.tvDetControlSalida.setText(objControl.getHorasalida());
            binding.tvDetControlEntrada.setText(objControl.getHoraentrada());
            binding.tvDetControlEst.setText(objControl.getEstado_Control_estadocId().toString());
            Glide.with(this)
                    .load(objControl.getImgv())
                    .signature(new ObjectKey(UUID.randomUUID().toString()))
                    .into(binding.ivDetControlImg);
        }

        if (!binding.tvDetControlEntrada.getText().toString().isEmpty()) {
            binding.btnDetControlEst.setVisibility(View.INVISIBLE);
        }

        binding.btnDetControlDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(DetalleControl.this);
                dialog.setTitle("ALERTA DE ELIMINACION");
                dialog.setMessage("Esta a punto de eliminar este control. Quieres continuar?");
                dialog.setCancelable(false);
                dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(DetalleControl.this, "Operacion cancelada", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Call<Control> call = ControlApiAdapter.getApiService().delControl(Integer.parseInt(binding.tvDetControlId.getText().toString()));
                        call.enqueue(new Callback<Control>() {
                            @Override
                            public void onResponse(Call<Control> call, Response<Control> response) {
                                if (response.isSuccessful()) {
                                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(), "Control eliminado con exito!", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Control> call, Throwable t) {
                            }
                        });
                    }
                });
                AlertDialog dialog1 = dialog.create();
                dialog1.show();
            }
        });

        binding.btnDetControlEst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(DetalleControl.this);
                dialog.setTitle("ALERTA DE CAMBIO DE ESTADO");
                dialog.setMessage("Esta a punto de terminar este control, es decir, que el vehiculo regreso a la sede. Quieres continuar?");
                dialog.setCancelable(false);
                dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(DetalleControl.this, "Operacion cancelada", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final TimeZone tz = TimeZone.getTimeZone("GMT-5");
                        final Calendar c = Calendar.getInstance(tz);
                        int mHour = c.get(Calendar.HOUR_OF_DAY);
                        int mMinute = c.get(Calendar.MINUTE);

                        horaE = String.format("%02d:%02d", mHour , mMinute);

                        Control cv = new Control();
                        Integer id = Integer.parseInt(binding.tvDetControlId.getText().toString());
                        cv.setVehiculo_vehiculoId(binding.tvDetControlVeh.getText().toString());
                        cv.setEmpleado_empleadoId(binding.tvDetControlEmp.getText().toString());
                        cv.setFecha(binding.tvDetControlFecha.getText().toString());
                        cv.setHorasalida(binding.tvDetControlSalida.getText().toString());
                        cv.setHoraentrada(horaE);
                        if (binding.tvDetControlEst.getText().toString().equals("1")) {
                            cv.setEstado_Control_estadocId(2);
                        } else {
                            cv.setEstado_Control_estadocId(1);
                        }

                        Call<Control> call = ControlApiAdapter.getApiService().updControl(id, cv);
                        call.enqueue(new Callback<Control>() {
                            @Override
                            public void onResponse(Call<Control> call, Response<Control> response) {
                                if(response.isSuccessful()) {
                                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(), "Control terminado con exito!", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Control> call, Throwable t) {
                            }
                        });
                    }
                });
                AlertDialog dialog1 = dialog.create();
                dialog1.show();
            }
        });

    }
}