package com.example.appafv.Fragments.Vehiculo;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.Utils.VehiculoApiAdapter;
import com.example.appafv.databinding.ActivityDetalleVehiculoBinding;
import java.util.UUID;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetalleVehiculo extends AppCompatActivity {

    private ActivityDetalleVehiculoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetalleVehiculoBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("detalleVehiculo")) {
            Vehiculo objVehiculo = this.getIntent().getParcelableExtra("detalleVehiculo");
            binding.tvDetVehId.setText(objVehiculo.getVehiculoId().toString());
            binding.tvDetVehPla.setText(objVehiculo.getPlaca());
            binding.tvDetVehProv.setText(objVehiculo.getProveedor_proveedorId());
            binding.tvDetVehPre.setText(objVehiculo.getPrecio().toString());
            binding.tvDetVehMar.setText(objVehiculo.getMarca());
            binding.tvDetVehAnio.setText(objVehiculo.getAnioFabri().toString());
            binding.tvDetVehUrl.setText(objVehiculo.getImgv());
            binding.tvDetVehObs.setText(objVehiculo.getObservacion());
            binding.tvDetVehEst.setText(objVehiculo.getEstado_Vehiculo_estadovId().toString());
            Glide.with(this)
                    .load(objVehiculo.getImgv())
                    .signature(new ObjectKey(UUID.randomUUID().toString()))
                    .into(binding.ivDetVehImg);
        }

        binding.btnRegresarDetVeh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetalleVehiculo.this, MenuPrincipalActivity.class);
                startActivity(intent);
                finish();
            }
        });

        binding.btnDetVehEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Vehiculo ve = new Vehiculo();
                ve.setVehiculoId(Integer.parseInt(binding.tvDetVehId.getText().toString()));
                ve.setProveedor_proveedorId(binding.tvDetVehProv.getText().toString());
                ve.setPlaca(binding.tvDetVehPla.getText().toString());
                ve.setPrecio(Double.parseDouble(binding.tvDetVehPre.getText().toString()));
                ve.setMarca(binding.tvDetVehMar.getText().toString());
                ve.setAnioFabri(Integer.parseInt(binding.tvDetVehAnio.getText().toString()));
                ve.setImgv(binding.tvDetVehUrl.getText().toString());
                ve.setObservacion(binding.tvDetVehObs.getText().toString());
                ve.setEstado_Vehiculo_estadovId(Integer.parseInt(binding.tvDetVehEst.getText().toString()));
                Intent editarV = new Intent(getApplicationContext(), EditarVehiculo.class);
                editarV.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                editarV.putExtra("editarVehiculo", ve);
                getApplicationContext().startActivity(editarV);
            }
        });

        binding.btnDetVehEst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Vehiculo ve = new Vehiculo();
                Integer id = Integer.parseInt(binding.tvDetVehId.getText().toString());
                ve.setProveedor_proveedorId(binding.tvDetVehProv.getText().toString());
                ve.setPlaca(binding.tvDetVehPla.getText().toString());
                ve.setPrecio(Double.parseDouble(binding.tvDetVehPre.getText().toString()));
                ve.setMarca(binding.tvDetVehMar.getText().toString());
                ve.setAnioFabri(Integer.parseInt(binding.tvDetVehAnio.getText().toString()));
                ve.setImgv(binding.tvDetVehUrl.getText().toString());
                ve.setObservacion(binding.tvDetVehObs.getText().toString());
                if (binding.tvDetVehEst.getText().equals("1")) {
                    ve.setEstado_Vehiculo_estadovId(2);
                } else {
                    ve.setEstado_Vehiculo_estadovId(1);
                }
                cambiarEstadoVehiculo(id, ve);
            }
        });
    }

    private void cambiarEstadoVehiculo(Integer id, Vehiculo ve) {
        Call<Vehiculo> call = VehiculoApiAdapter.getApiService().updVehiculo(id, ve);
        call.enqueue(new Callback<Vehiculo>() {
            @Override
            public void onResponse(Call<Vehiculo> call, Response<Vehiculo> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Cambio de estado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Vehiculo> call, Throwable t) {
            }
        });
    }

}