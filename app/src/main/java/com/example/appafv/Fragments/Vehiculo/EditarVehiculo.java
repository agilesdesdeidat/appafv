package com.example.appafv.Fragments.Vehiculo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.example.appafv.MenuPrincipalActivity;
import com.example.appafv.Model.Proveedor;
import com.example.appafv.Model.Vehiculo;
import com.example.appafv.Utils.ProveedorApiAdapter;
import com.example.appafv.Utils.VehiculoApiAdapter;
import com.example.appafv.databinding.ActivityEditarVehiculoBinding;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarVehiculo extends AppCompatActivity {

    private ActivityEditarVehiculoBinding binding;
    private int IMG_REQUEST = 21;
    private Bitmap bitmap;
    private int editImage = 0;

    String nombreProvV;
    String marcaV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEditarVehiculoBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        if (this.getIntent().hasExtra("editarVehiculo")) {
            Vehiculo objVehiculo = this.getIntent().getParcelableExtra("editarVehiculo");
            binding.tvEditVehId.setText(objVehiculo.getVehiculoId().toString());
            binding.etEditVehPla.setText(objVehiculo.getPlaca());
            nombreProvV = objVehiculo.getProveedor_proveedorId();
            binding.etEditVehPre.setText(objVehiculo.getPrecio().toString());
            binding.etEditVehAnio.setText(objVehiculo.getAnioFabri().toString());
            marcaV = objVehiculo.getMarca();
            binding.etEditVehUrl.setText(objVehiculo.getImgv());
            binding.etEditVehObs.setText(objVehiculo.getObservacion());
            binding.tvEditVehEst.setText(objVehiculo.getEstado_Vehiculo_estadovId().toString());
            Glide.with(this)
                    .load(objVehiculo.getImgv())
                    .signature(new ObjectKey(UUID.randomUUID().toString()))
                    .into(binding.ivEditVehImg);
        }

        obtenerNombreProveedor();
        obtenerMarcas();

        binding.ibEditCargaUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editImage = 1;

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMG_REQUEST);
            }
        });

        binding.btnEditVeh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etEditVehPla.getText().toString().isEmpty() || binding.etEditVehPla.getText().toString().length() != 7 ||
                        binding.etEditVehPre.getText().toString().isEmpty() || binding.etEditVehAnio.getText().toString().isEmpty() ||
                        binding.ivEditVehImg.getDrawable() == null) {
                    validarDatos();
                } else {
                    Vehiculo veh = new Vehiculo();
                    Integer id = Integer.parseInt(binding.tvEditVehId.getText().toString());
                    veh.setPlaca(binding.etEditVehPla.getText().toString());
                    veh.setProveedor_proveedorId(binding.spEditVehProv.getSelectedItem().toString());
                    veh.setPrecio(Double.parseDouble(binding.etEditVehPre.getText().toString()));
                    veh.setAnioFabri(Integer.parseInt(binding.etEditVehAnio.getText().toString()));
                    veh.setMarca(binding.spEditVehMarca.getSelectedItem().toString());
                    veh.setObservacion(binding.etEditVehObs.getText().toString());
                    veh.setEstado_Vehiculo_estadovId(Integer.parseInt(binding.tvEditVehEst.getText().toString()));

                    if (editImage == 1) {
                        veh.setImgv(uploadImage());
                    } else {
                        veh.setImgv(binding.etEditVehUrl.getText().toString());
                    }

                    updVehiculo(id, veh);
                }
            }
        });

        binding.btnCanVeh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditarVehiculo.this, MenuPrincipalActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                binding.ivEditVehImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void updVehiculo(Integer id, Vehiculo veh) {
        Call<Vehiculo> call = VehiculoApiAdapter.getApiService().updVehiculo(id, veh);
        call.enqueue(new Callback<Vehiculo>() {
            @Override
            public void onResponse(Call<Vehiculo> call, Response<Vehiculo> response) {
                if(response.isSuccessful()) {
                    Intent intent = new Intent(getApplicationContext(), MenuPrincipalActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Vehiculo actualizado con exito!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Vehiculo> call, Throwable t) {
            }
        });
    }

    private void obtenerMarcas() {
        List<String> listm = new ArrayList<>();
        listm.add(marcaV);
        listm.add("Chevrolet");
        listm.add("Hyundai");
        listm.add("Mercedes Benz");
        listm.add("Audi");
        listm.add("Nissan");
        listm.add("Kia");
        ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, listm);
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spEditVehMarca.setAdapter(spArrayAdapter);
    }

    private void obtenerNombreProveedor() {
        Call<ArrayList<Proveedor>> call = ProveedorApiAdapter.getApiService().getProveedores();
        call.enqueue(new Callback<ArrayList<Proveedor>>() {
            @Override
            public void onResponse(Call<ArrayList<Proveedor>> call, Response<ArrayList<Proveedor>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Proveedor> proveedorNom = response.body();
                    List<String> list = new ArrayList<String>();
                    list.add(nombreProvV);
                    for (Proveedor p : proveedorNom) {
                        list.add(p.getNombreEmp());
                    }
                    ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    binding.spEditVehProv.setAdapter(spArrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Proveedor>> call, Throwable t) {
            }
        });
    }

    private String uploadImage() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);

        return encodedImage;
    }

    private void validarDatos() {
        Pattern patternPlaca = Pattern.compile("^[A-Z0-9]{3}-[A-Z0-9]{3}$");
        Matcher matcherPlaca = patternPlaca.matcher(binding.etEditVehPla.getText().toString());

        if (binding.etEditVehPla.getText().toString().isEmpty()) {
            binding.etEditVehPla.setError("Este campo no puede estar vacio.");
        } else if (!matcherPlaca.find()) {
            binding.etEditVehPla.setError("Ingrese una placa valida.");
        } else if (binding.etEditVehPre.getText().toString().isEmpty()) {
            binding.etEditVehPre.setError("Este campo no puede estar vacio.");
        } else if (binding.etEditVehAnio.getText().toString().isEmpty()) {
            binding.etEditVehAnio.setError("Este campo no puede estar vacio.");
        } else if (binding.etEditVehAnio.getText().toString().length() != 4) {
            binding.etEditVehAnio.setError("Este campo debe contener 4 caracteres.");
        } else if (binding.ivEditVehImg.getDrawable() == null) {
            Toast.makeText(getApplicationContext(), "Debe elegir una imagen.", Toast.LENGTH_SHORT).show();
        }
    }

}
