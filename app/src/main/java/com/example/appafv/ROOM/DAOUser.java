package com.example.appafv.ROOM;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;

@Dao
public interface DAOUser {
    @Query("SELECT * FROM Entidad_User")
    List<Entidad_User> registro();

    @Query("SELECT usuario FROM Entidad_User")
    public String usuario();

    @Query("UPDATE Entidad_User SET usuario=:Usuario")
    public int updateUsuario(String Usuario);

    @Insert
    long insert(Entidad_User entidad_user);
}
