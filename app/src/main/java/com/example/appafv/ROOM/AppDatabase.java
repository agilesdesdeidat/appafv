package com.example.appafv.ROOM;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Entidad_User.class}, version =1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DAOUser daoUser();
}
