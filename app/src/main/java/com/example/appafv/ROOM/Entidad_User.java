package com.example.appafv.ROOM;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Entidad_User {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String usuario;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
